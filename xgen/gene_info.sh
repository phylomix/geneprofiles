#-- blastp_copy

export PGHOST=localhost
export PGPORT=5497
export PGUSER=phylomix

PSQL=psql

SCHEMA=phylo
SPECIES_TYPE=$SCHEMA.species_type
GENE_INFO=$SCHEMA.gene_info

# creating schema
$PSQL <<EOF
CREATE SCHEMA IF NOT EXISTS $SCHEMA;

do ' begin
  CREATE TYPE $SPECIES_TYPE as enum ();
exception
  when duplicate_object then null;
end ';


--drop table if exists $GENE_INFO;
create table if not exists $GENE_INFO (
    species $SPECIES_TYPE
    ,gene text
    ,location text
    ,source text[]
    ,protein text
    ,isoforms text
    ,protein_ids text[] 
);
EOF

BLAST_EXT=.blastp.tsv

#-- copying data
for blast_file in `ls ../blastp/*$BLAST_EXT`
do
  species=$(basename $blast_file $BLAST_EXT)
  echo $blast_file --">>" $species
  blast_all=$SCHEMA.${species}_blastp
  blast_in_species=$SCHEMA.${species}_blastp_in_species
  blast_cross_species=$SCHEMA.${species}_blastp_cross_species
  gene_info=$SCHEMA."${species}_gene"

# creating gene to protein info table
$PSQL <<EOF
  -- adding species_type
  alter type $SPECIES_TYPE add value if not exists '$species';

  -- gene_info
  BEGIN;
  DROP TABLE IF EXISTS $gene_info;
  CREATE TABLE $gene_info AS
  with x as (
  SELECT
    species::text::$SPECIES_TYPE
    ,attr->>'gene' as gene
    ,seqid
    ,source
    ,format('%s%s..%s',strand,start,stop) AS region
    ,attr->>'protein_id' as protein_id
    ,case when attr->>'product' ~ 'isoform'
      then substring(attr->>'product', '(.*) isoform')
      else attr->>'product'
     end protein
    ,coalesce(substring(attr->>'product', 'isoform (.*)'),'.') isoform
  FROM gff3."$species"
  WHERE seqtype = 'CDS'
  ), y as (
  select
    species
    ,gene
    ,seqid
    ,array_agg(distinct region) region
    ,array_agg(distinct source) source
    ,protein
    ,string_agg(DISTINCT isoform, ',' ORDER BY isoform) isoforms
    ,array_agg(DISTINCT protein_id) protein_ids
FROM x
GROUP BY species,gene,seqid,protein
)
select
  species
  ,gene
  ,string_agg(seqid||':'||region::text, ',') AS location
  ,source
  ,protein
  ,isoforms
  ,protein_ids
FROM y
GROUP BY species,gene,source,protein,isoforms,protein_ids
;
\d $GENE_INFO
\d $gene_info

\echo alter table $gene_info inherit $GENE_INFO;
alter table $gene_info inherit $GENE_INFO;
--CREATE INDEX ON $gene_info using gin(protein_ids);

COMMIT;
--ROLLBACK;

EOF
done

