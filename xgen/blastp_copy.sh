#-- blastp_copy

export PGHOST=localhost
export PGPORT=5497
export PGUSER=phylomix

PSQL=psql

SCHEMA=phylo
SPECIES_TYPE=$SCHEMA.species_type
GENE_INFO=$SCHEMA.gene_info

# creating schema
$PSQL <<EOF
CREATE SCHEMA IF NOT EXISTS $SCHEMA;
CREATE TABLE IF NOT EXISTS $SCHEMA.blastp_template (
  qseqid text
  ,sseqid text
  ,pident float
  ,length int
  ,mismatch int
  ,gapopen int
  ,qstart int
  ,qend int
  ,sstart int
  ,send int
  ,evalue float
  ,bitscore float
);

EOF

BLAST_EXT=.blastp.tsv

#-- copying data
for blast_file in `ls ../blastp/*$BLAST_EXT`
do
  species=$(basename $blast_file $BLAST_EXT)
  echo $blast_file --">>" $species
  blastp_all=$SCHEMA.${species}_blastp
  blastp_gene=$SCHEMA.${species}_blastp_gene
  blastp_in_species=$SCHEMA.${species}_blastp_in_species
  blastp_cross_species=$SCHEMA.${species}_blastp_cross_species
  dup_genes=$SCHEMA.${species}_duplicate_genes
  dup_gene_summary=$SCHEMA.${species}_duplicate_genes_summary

# copying blast data
  grep -hv '^#' $blast_file|
  awk '$3>=20 && $11<1e-10'|
  sed "s/gi|[0-9]*|//g" |
  sed 's/ref|\([^|]*\)|/\1/g'|
  $(PSQL) <<EOF
  BEGIN;

  create temp table if not exists $blastp_all () inherits (blastp_template);
  copy $blastp_all from stdin;

  drop table if exists $blastp_in_species;
  with r as (
    delete from $blastp_all b
      join gff3.gene_info q on array[qseqid] && q.protein_ids
      join gff3.gene_info s on array[sseqid] && s.protein_ids
     where q.species = s.species
     returning 
         q.species q_species
        ,s.species s_species
        ,q.accession as q_contig
        ,s.accession as s_contig
        ,q.gene q_gene
        ,s.gene s_gene
        ,q.protein q_protein
        ,s.protein s_protein
        ,b.*
  )
  create table $blastp_in_species as
  select distinct *
    from r
   where qseqid <> sseqid
     and (q_contig,q_gene)<>(s_contig,s_gene);
  comment on table $blastp_in_species is 'blast hits above threshold';

  CREATE INDEX ON $blastp_in_species(qseqid);
  CREATE INDEX ON $blastp_in_species(sseqid);

  drop table if exists $blastp_cross_species;
  with r as (
    delete from $blastp_all b
      join gff3.gene_info q on array[qseqid] && q.protein_ids
      join gff3.gene_info s on array[sseqid] && s.protein_ids
     where q.species = s.species
     returning 
         q.species q_species
        ,s.species s_species
        ,q.accession as q_contig
        ,s.accession as s_contig
        ,q.gene q_gene
        ,s.gene s_gene
        ,q.protein q_protein
        ,s.protein s_protein
        ,b.*
  ),m as (
    select qseqid
          ,max(pident) max_pident
	  ,min(evalue) min_evalue
      from r
     group by qseqid
  )
  create table $blastp_cross_species as
  select distinct *
    from m
    join r using(qseqid)
   where evalue=min_evalue;
  comment on table $blastp_cross_species is 'blast hits above threshold';

  CREATE INDEX ON $blastp_cross_species(qseqid);
  CREATE INDEX ON $blastp_cross_species(sseqid);

  \echo select * from $blastp_cross_species limit 1;
  select * from $blastp_cross_species limit 1;
  \echo select count(*) from $blastp_cross_species;
  select count(*) from $blastp_cross_species;

  \echo dup gene list
  drop table if exists $dup_genes;

  create table $dup_genes as
  select w.*
        ,x.sseqid      x_seqid
        ,x.s_species   x_species
        ,x.s_contig    x_contig
        ,x.s_gene      x_gene
        ,x.s_protein   x_protein
        ,x.length      x_length
        ,x.qlen        x_qlen
        ,x.slen        x_slen
        ,x.mismatch    x_mismatch
        ,x.gapopen     x_gapopen
        ,x.gaps        x_gaps
        ,x.bitscore    x_bitscore
        ,x.max_pident
        ,x.max_qcovs
        ,x.min_evalue
    from $blastp_in_species w
    join $blastp_cross_species x using (qseqid)
   where w.pident >= x.max_pident
     and w.evalue <= x.min_evalue
  ;

  \echo duplicate summary
  comment on table $dup_genes is 'duplicated gene list';
  create index on $dup_genes (qseqid);
  select * from $dup_genes limit 1;
  select count(*) from $dup_genes;

  -- dup gene aggregates
  drop table if exists $dup_g_agg;
  create table $dup_g_agg as
  select array_agg(distinct q_gene)      q_gene
        ,array_agg(distinct s_gene)      s_gene
        ,array_agg(distinct x_gene)      x_gene
        ,array_agg(distinct q_contig)    q_contig
        ,array_agg(distinct s_contig)    s_contig
        ,array_agg(distinct x_contig)    x_contig
        ,qseqid
        ,array_agg(distinct sseqid)      sseqid
        ,array_agg(distinct x_seqid)     x_seqid
        ,array_agg(distinct q_protein)   q_protein
        ,array_agg(distinct s_protein)   s_protein
        ,array_agg(distinct x_protein)   x_protein
        ,array_agg(distinct pident)      pident
        ,array_agg(distinct max_pident)  x_pident
        ,array_agg(distinct length)      length
        ,array_agg(distinct x_length)    x_length
        ,array_agg(distinct mismatch)    mismatch
        ,array_agg(distinct x_mismatch)  x_mismatch
        ,array_agg(distinct gapopen)     gapopen
        ,array_agg(distinct x_gapopen)   x_gapopen
        ,array_agg(distinct evalue)      evalue
        ,array_agg(distinct min_evalue)  m_evalue
        ,array_agg(distinct bitscore)    bitscore
        ,array_agg(distinct x_bitscore)  x_bitscore
        ,array_agg(distinct qlen)        qlen
        ,array_agg(distinct slen)        slen
        ,array_agg(distinct x_qlen)      x_qlen
        ,array_agg(distinct x_slen)      x_slen
        ,array_agg(distinct gaps)        gaps
        ,array_agg(distinct x_gaps)      x_gaps
--        ,array_agg(distinct qcovs)       qcovs
--        ,array_agg(distinct max_qcovs)   x_qcovs
    from $dup_genes
   group by qseqid
   ;
   comment on table $dup_g_agg is 'duplicated gene aggregates';
  select * from $dup_g_agg limit 1;
  select count(*) from $dup_g_agg;


  -- singleton
  drop table if exists $singleton;
  create table $singleton as
  select *
    from $x_species x
   where qseqid not in (
     select qseqid from $win_species
   );

  COMMIT;
EOF

done
