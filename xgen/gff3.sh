#!/bin/bash

#PGDATABASE
export PGHOST=localhost
export PGPORT=5497
export PGUSER=phylomix

SCHEMA=gff3
TEMPLATE=$SCHEMA.template
READER=$SCHEMA.reader
SPECIES_TYPE=species_type

psql <<EOF
BEGIN;

\echo $SCHEMA
CREATE SCHEMA IF NOT EXISTS $SCHEMA;

\echo creating $TEMPLATE
CREATE TABLE IF NOT EXISTS $TEMPLATE (
  species $SPECIES_TYPE
  ,seqid TEXT
  ,source TEXT
  ,seqtype TEXT
  ,start INT
  ,stop  INT
  ,score TEXT
  ,strand CHAR
  ,phase  CHAR
  ,attr JSONB
);

/*
\echo creating $READER
CREATE TABLE IF NOT EXISTS $READER () INHERITS ( $TEMPLATE );

\echo created $READER
SELECT count(*) FROM $READER;
--DELETE FROM $READER;
--SELECT count(*) FROM $READER;
*/
COMMIT;
EOF

# copy gff into :reader

#GFF=$(ls -t ../*/GFF/ref*_top_level.gff3.gz | head -1)
for species_gff in `ls -dt ../*/GFF`
#for species_gff in `ls -dt ../Drosophila_takahashii/GFF`
do
  if ls $species_gff/ref_*_top_level.gff3.gz 2>/dev/null; then
    :
  else
    echo 'No gff:' $species_gff
    continue
  fi
  gff=$(ls -t $species_gff/ref_*_top_level.gff3.gz|head -1)
  species=${gff#*/}
  species=${species%%/*}
  echo $gff '-->' $species
  [[ -z "$species" ]] && exit
  gff_table=$SCHEMA.\"${species}\"
  echo "-------------------------------------------------"
  echo "creating $gff_table"
  psql -c "alter type $SPECIES_TYPE add value if not exists '$species'";
  gzip -cd $gff | awk '/^#!annotation-source/ || $3=="CDS"' |
  	./gff3tojson.pl | psql -c "
    BEGIN;
	CREATE TABLE IF NOT EXISTS $gff_table () INHERITS ( $TEMPLATE );
	DELETE FROM $gff_table;
	COPY $gff_table FROM STDIN;
    COMMIT;"
  psql -c "SELECT count(*) FROM $gff_table;"
done

exit;
psql<<EOF
/*
DO $$ BEGIN
  EXECUTE(
    SELECT format('CREATE TYPE seqtype_enum AS ENUM (%s)',
                  ,string_agg(DISTINCT quote_literal(seqtype),', '))
    FROM :reader);	
END $$
*/
EOF

--COMMIT PREPARED;

psql -c 'CREATE INDEX ON $READER (seqtype);'
psql -c 'CREATE INDEX ON $READER (attr);'

