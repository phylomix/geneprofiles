#!/usr/bin/perl

use DBI;
use feature 'say';

my $user = 'phylomix';
my $dbname=$user;
binmode STDOUT, ":utf8";

my $dbh = DBI->connect("dbi:Pg:dbname=phylomix", 'phylomix', '', {AutoCommit => 0});

my $sql = <<EOF;
SELECT scientific_name
     , string_agg(DISTINCT common_name, '; ')
  FROM species_dic
  WHERE ascii(common_name)>=128
    AND scientific_name = '%s'
  GROUP BY scientific_name
    ;
EOF

# main
#my $species=shift;
while (<>) { # taxonomy.list
    my ($species, $common, @taxonomy) = split /\t/;
    my $query = sprintf $sql, $species;
    my $res = $dbh->selectall_arrayref($query);
    my $ja = $res->[0][1] || '-';
    print join "\t", $species, $common, $ja, @taxonomy;
}

$dbh->disconnect;
