psql -U phylomix <<EOF
DROP TABLE identical_seq;
CREATE TABLE identical_seq AS
WITH a AS (
  SELECT DISTINCT seq_md5, count(*)
    FROM acc_seq_md5
   GROUP BY seq_md5
  HAVING count(*)>1
)
SELECT DISTINCT seq_md5
     , lsh AS nilsimsa
     , count(DISTINCT phylum) AS phylum_cnt
     , count(DISTINCT class) AS class_cnt
     , count(DISTINCT "order") AS order_cnt
     , count(DISTINCT family) AS family_cnt
     , count(DISTINCT scientific_name) AS species_cnt
     , count(*) AS sequence_cnt
     , string_agg(DISTINCT annotation, '; ' ORDER BY annotation) AS annotation
     , string_agg(DISTINCT phylum || '; ' || class || '; ' || "order" || '; '
			 || family || '; ' || scientific_name || ' # ' || common_name,
			 E'\n'
     		 ORDER BY phylum || '; ' || class || '; ' || "order" || '; '
			 || family || '; ' || scientific_name || ' # ' || common_name )
		AS species
     , string_agg(DISTINCT accession, ' ' ORDER BY accession) AS accession
FROM a
NATURAL LEFT JOIN acc_seq_md5
JOIN nilsimsa ON seq_md5=md5
NATURAL JOIN seq_info
NATURAL JOIN genome_master
GROUP BY seq_md5, nilsimsa
ORDER BY phylum_cnt, class_cnt, order_cnt, species_cnt, count(*) DESC;
EOF
