        
psql -U phylomix <<'EOF'
CREATE OR REPLACE FUNCTION dist(a1 TEXT, a2 TEXT) 
RETURNS INT
IMMUTABLE
LANGUAGE plpgsql AS $$
DECLARE b1 bit varying := to_bits(a1);
DECLARE b2 bit varying := to_bits(a2);
BEGIN
  RETURN length(replace((b1 # b2)::text, '0', ''));
END
$$
EOF
