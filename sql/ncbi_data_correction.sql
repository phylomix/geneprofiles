UPDATE genome_master
   SET phylum='Chordata'
     , class='Mammalia'
     , "order"='Primates'
     , family='Hominidae'
 WHERE scientific_name='H sapiens'
;

UPDATE genome_master
   SET phylum='Chordata'
     , class='Mammalia'
     , "order"='Rodentia'
     , family='Muridae'
 WHERE scientific_name='R norvegicus'
    OR scientific_name='M musculus'
;

UPDATE genome_master
   SET phylum='Chordata'
     , class='Actinopterygii'
     , "order"='Cypriniformes'
     , family='Cyprinidae'
 WHERE scientific_name='D rerio'
;

UPDATE genome_master
   SET phylum='Actinopterygii'
 WHERE phylum='Actinopteri'
;

--select * FROM genome_data_master WHERE scientific_name like 'Drosophila%';
UPDATE genome_data_master gm
   SET ncbi=''
 WHERE NOT EXISTS (
    SELECT 1 FROM genome_stats
       WHERE scientific_name=gm.scientific_name )
;

UPDATE genome_data_master gm
   SET ncbi='S'
 WHERE EXISTS (
    SELECT 1 FROM genome_stats
       WHERE scientific_name=gm.scientific_name )
;

