CREATE FUNCTION taxid (n TEXT)
RETURNS INT AS $$
SELECT tax_id FROM tax_names WHERE name_txt=n
$$ LANGUAGE sql;

