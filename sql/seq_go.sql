
psql -U phylomix <<EOF

CREATE TABLE seq_md5_go (
  seq_md5 text PRIMARY KEY
  , goterm_id text
);

CREATE TEMP TABLE tmp (like seq_md5_go);

\copy tmp FROM PROGRAM 'awk -F$"\t" "NF==15 && \\\$14!=\\"\\"{OFS=\\"\\t\\";print \\\$2,\\\$14}" */*.tsv | uniq | sort -u'

INSERT INTO seq_md5_go
SELECT seq_md5, string_agg(DISTINCT goterm_id, ' ' ORDER BY goterm_id)
FROM tmp
GROUP BY seq_md5;
EOF
