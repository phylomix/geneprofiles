psql -U phylomix <<'EOF'
CREATE OR REPLACE FUNCTION to_bits( b BYTEA )
RETURNS bit varying
LANGUAGE plpgsql
AS $$
DECLARE n INT := octet_length(b) -1;
DECLARE i INT;
DECLARE ret bit varying = '';
BEGIN
  FOR i IN 0..n LOOP
    ret := ret || get_byte(b, i)::bit(8);
--    RAISE NOTICE 'b % i % n % ret % r %', b, i, n, ret, get_byte(b, i)::bit(8);
  END LOOP;
  RETURN ret;
END
$$;

CREATE OR REPLACE FUNCTION to_bits( TEXT )
RETURNS bit varying
LANGUAGE sql
AS $$
SELECT to_bits( decode($1, 'base64') );
$$;

CREATE OR REPLACE FUNCTION to_bits( INT )
RETURNS bit varying
LANGUAGE sql
AS $$
SELECT $1::bit(1);
$$;
EOF
