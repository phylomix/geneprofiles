--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.4
-- Dumped by pg_dump version 9.5.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- Name: phyla_enum; Type: TYPE; Schema: public; Owner: phylomix
--

CREATE TYPE phyla_enum AS ENUM (
    'Chordata',
    'Hemichordata',
    'Echinodermata',
    'Priapulida',
    'Nematoda',
    'Arthropoda',
    'Platyhelminthes',
    'Brachiopoda',
    'Mollusca',
    'Annelida',
    'Cnidaria',
    'Placozoa',
    'Porifera',
    'Opisthokonta',
    'Ascomycota',
    'Streptophyta'
);


ALTER TYPE phyla_enum OWNER TO phylomix;

--
-- Name: seq_feature_enum; Type: TYPE; Schema: public; Owner: phylomix
--

CREATE TYPE seq_feature_enum AS ENUM (
    'mRNA',
    'operon',
    'CDS',
    'gene',
    'three_prime_UTR',
    'five_prime_UTR'
);


ALTER TYPE seq_feature_enum OWNER TO phylomix;

--
-- Name: aa_seq_partial_id_match(text); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION aa_seq_partial_id_match(str text) RETURNS TABLE("like" intensity.aa_seq)
    LANGUAGE sql
    AS $$
select * from aa_seq where array_to_string(names,' ') ~ str;
$$;


ALTER FUNCTION public.aa_seq_partial_id_match(str text) OWNER TO phylomix;

--
-- Name: aa_seq_spk(text); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION aa_seq_spk(name text) RETURNS integer
    LANGUAGE sql
    AS $$
select spk from aa_seq where array[name] <@ names; $$;


ALTER FUNCTION public.aa_seq_spk(name text) OWNER TO phylomix;

--
-- Name: aa_seq_to_fasta(text); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION aa_seq_to_fasta(prefix text) RETURNS TABLE(name text, data text)
    LANGUAGE sql
    AS $$
SELECT '>' ||  array_to_string(names,',') AS name, data
  FROM aa_seq
  WHERE array_to_string(names,',') ~ prefix;
$$;


ALTER FUNCTION public.aa_seq_to_fasta(prefix text) OWNER TO phylomix;

--
-- Name: annot_text(integer); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION annot_text(seqspk integer) RETURNS text
    LANGUAGE sql
    AS $$ select note from annot where aa_seq_spk = seqspk; $$;


ALTER FUNCTION public.annot_text(seqspk integer) OWNER TO phylomix;

--
-- Name: annot_text(text); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION annot_text(name text) RETURNS text
    LANGUAGE sql
    AS $$ select note from annot where id = name; $$;


ALTER FUNCTION public.annot_text(name text) OWNER TO phylomix;

--
-- Name: annotation_note_md5(); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION annotation_note_md5() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  NEW.note_md5 := md5(OLD.note)::BYTEA;
END
$$;


ALTER FUNCTION public.annotation_note_md5() OWNER TO phylomix;

--
-- Name: annotation_of(text); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION annotation_of(identifier text) RETURNS TABLE(id text, note text, names text[])
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
    SELECT identifier, note, names
      FROM aa_seq
      JOIN annotation ON aa_seq.spk = aa_seq_spk
     WHERE names @> ARRAY[identifier];
END
$$;


ALTER FUNCTION public.annotation_of(identifier text) OWNER TO phylomix;

--
-- Name: annotation_profile(text); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION annotation_profile(a text) RETURNS TABLE(common_name text, "seq/sp" bigint, species text, "sp/f" bigint, family text, "f/o" bigint, "order" text, "o/c" bigint, class text, "c/p" bigint, phylum phyla_enum)
    LANGUAGE sql
    AS $$
SELECT DISTINCT
       common_name
     , count(gi) over (partition BY species) as seq_sp
     , species
     , count(species) over (partition BY family) as sp_f
     , family
     , count(family) over (partition BY "order") as f_o
     , "order"
     , count("order") over (partition BY phylum,class) as o_c
     , class
     , count(class) over (partition BY phylum) as c_p
     , phylum
  FROM (SELECT *, scientific_name AS species FROM seq_info1 WHERE annotation=a) x
  JOIN genome_master3 USING (scientific_name)
  ORDER BY c_p DESC, o_c DESC, f_o DESC, sp_f DESC, seq_sp DESC
;
$$;


ALTER FUNCTION public.annotation_profile(a text) OWNER TO phylomix;

--
-- Name: base64_to_bits(text); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION base64_to_bits(s text) RETURNS bit varying
    LANGUAGE sql IMMUTABLE
    AS $$
SELECT bytea_to_bits(decode(s,'base64'));
$$;


ALTER FUNCTION public.base64_to_bits(s text) OWNER TO phylomix;

--
-- Name: bitcount(bytea); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION bitcount(bits bytea) RETURNS integer
    LANGUAGE plpgsql
    AS $$
  DECLARE i INT;
  DECLARE j INT;
  DECLARE m INT;
  DECLARE n INT;
  DECLARE L INT;
  BEGIN
    L := octet_length(bits);
    n := 0;
    FOR j IN 1..L LOOP
      m := get_byte(bits, j-1);
      FOR i IN 0..15 LOOP
        n := n + (m & 1); 
        m := m >> 1;
      END LOOP;
    END LOOP;
    RETURN n;
  END
$$;


ALTER FUNCTION public.bitcount(bits bytea) OWNER TO phylomix;

--
-- Name: bitcount(integer); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION bitcount(b integer) RETURNS integer
    LANGUAGE plpgsql IMMUTABLE
    AS $$
BEGIN
  b = (b & x'55'::int) + (b >> 1 & x'55'::int);
  b = (b & x'33'::int) + (b >> 2 & x'33'::int);
  RETURN (b & x'0f'::int) + (b >> 4 & x'0f'::int);
END
$$;


ALTER FUNCTION public.bitcount(b integer) OWNER TO phylomix;

--
-- Name: bitcount(bit varying); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION bitcount(b bit varying) RETURNS integer
    LANGUAGE plpgsql IMMUTABLE
    AS $$
--DECLARE n integer := octet_length(b)-1;
DECLARE n integer := bit_length(b)-1;
DECLARE i integer;
DECLARE c integer := 0;
BEGIN
  FOR i IN 0..n LOOP
    c := c + get_bit(b, i);
  END LOOP;
  RETURN c;
END
$$;


ALTER FUNCTION public.bitcount(b bit varying) OWNER TO phylomix;

--
-- Name: blastp_query(text); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION blastp_query(query text) RETURNS TABLE(q_names text, s_names text, n_members integer, note text, identity double precision, len integer, mismatch integer, gap_opens integer, q_start integer, q_end integer, s_start integer, s_end integer, evalue double precision, bit_score double precision)
    LANGUAGE sql
    AS $$
  SELECT array_to_string(q.names, '; ') AS q_names,
	 array_to_string(s.names, '; ') AS s_names,
	 c.n_members,
	 a.note,
       identity, len, mismatch, gap_opens,
       q_start, q_end, s_start, s_end,
       evalue, bit_score
  FROM aa_seq q
  LEFT JOIN blastp ON q.spk=q_seq_spk
  LEFT JOIN aa_seq s ON s.spk=s_seq_spk
  LEFT JOIN cluster c ON c.aa_seq_spk IN (q_seq_spk, s_seq_spk)
  LEFT JOIN annot a ON a.aa_seq_spk IN (q_seq_spk, s_seq_spk)
  WHERE array_to_string(q.names, '; ') ~ query
$$;


ALTER FUNCTION public.blastp_query(query text) OWNER TO phylomix;

--
-- Name: bytea_to_bits(bytea); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION bytea_to_bits(s bytea) RETURNS bit varying
    LANGUAGE plpgsql IMMUTABLE
    AS $$
DECLARE n int := octet_length(s)-1;
DECLARE i int;
DECLARE r bit varying := '';
BEGIN
  for i IN 0..n LOOP
    r := r || get_byte(s, i)::bit(8);
  END LOOP;
  RETURN r;
END
$$;


ALTER FUNCTION public.bytea_to_bits(s bytea) OWNER TO phylomix;

--
-- Name: bytea_to_bits(text); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION bytea_to_bits(s text) RETURNS bit varying
    LANGUAGE sql IMMUTABLE
    AS $$
SELECT bytea_to_bits(decode(s,'hex'));
$$;


ALTER FUNCTION public.bytea_to_bits(s text) OWNER TO phylomix;

--
-- Name: colpivot(character varying, character varying, character varying[], character varying[], character varying, character varying); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION colpivot(out_table character varying, in_query character varying, key_cols character varying[], class_cols character varying[], value_e character varying, col_order character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
    declare
        in_table varchar;
        col varchar;
        ali varchar;
        on_e varchar;
        i integer;
        rec record;
        query varchar;
        -- This is actually an array of arrays but postgres does not support an array of arrays type so we flatten it.
        -- We could theoretically use the matrix feature but it's extremly cancerogenous and we would have to involve
        -- custom aggrigates. For most intents and purposes postgres does not have a multi-dimensional array type.
        clsc_cols text[] := array[]::text[];
        n_clsc_cols integer;
        n_class_cols integer;
    begin
        in_table := quote_ident('__' || out_table || '_in');
        execute ('create temp table ' || in_table || ' on commit drop as ' || in_query);
        -- get ordered unique columns (column combinations)
        query := 'select array[';
        i := 0;
        foreach col in array class_cols loop
            if i > 0 then
                query := query || ', ';
            end if;
            query := query || 'quote_literal(' || quote_ident(col) || ')';
            i := i + 1;
        end loop;
        query := query || '] x from ' || in_table;
        for j in 1..2 loop
            if j = 1 then
                query := query || ' group by ';
            else
                query := query || ' order by ';
                if col_order is not null then
                    query := query || col_order || ' ';
                    exit;
                end if;
            end if;
            i := 0;
            foreach col in array class_cols loop
                if i > 0 then
                    query := query || ', ';
                end if;
                query := query || quote_ident(col);
                i := i + 1;
            end loop;
        end loop;
        -- raise notice '%', query;
        for rec in
            execute query
        loop
            clsc_cols := array_cat(clsc_cols, rec.x);
        end loop;
        n_class_cols := array_length(class_cols, 1);
        n_clsc_cols := array_length(clsc_cols, 1) / n_class_cols;
        -- build target query
        query := 'select ';
        i := 0;
        foreach col in array key_cols loop
            if i > 0 then
                query := query || ', ';
            end if;
            query := query || '_key.' || quote_ident(col) || ' ';
            i := i + 1;
        end loop;
        for j in 1..n_clsc_cols loop
            query := query || ', ';
            col := '';
            for k in 1..n_class_cols loop
                if k > 1 then
                    col := col || ', ';
                end if;
                col := col || clsc_cols[(j - 1) * n_class_cols + k];
            end loop;
            ali := '_clsc_' || j::text;
            query := query || '(' || replace(value_e, '#', ali) || ')' || ' as ' || quote_ident(col) || ' ';
        end loop;
        query := query || ' from (select distinct ';
        i := 0;
        foreach col in array key_cols loop
            if i > 0 then
                query := query || ', ';
            end if;
            query := query || quote_ident(col) || ' ';
            i := i + 1;
        end loop;
        query := query || ' from ' || in_table || ') _key ';
        for j in 1..n_clsc_cols loop
            ali := '_clsc_' || j::text;
            on_e := '';
            i := 0;
            foreach col in array key_cols loop
                if i > 0 then
                    on_e := on_e || ' and ';
                end if;
                on_e := on_e || ali || '.' || quote_ident(col) || ' = _key.' || quote_ident(col) || ' ';
                i := i + 1;
            end loop;
            for k in 1..n_class_cols loop
                on_e := on_e || ' and ';
                on_e := on_e || ali || '.' || quote_ident(class_cols[k]) || ' = ' || clsc_cols[(j - 1) * n_class_cols + k];
            end loop;
            query := query || 'left join ' || in_table || ' as ' || ali || ' on ' || on_e || ' ';
        end loop;
        -- raise notice '%', query;
        execute ('create temp table ' || quote_ident(out_table) || ' on commit drop as ' || query);
        -- cleanup temporary in_table before we return
        execute ('drop table ' || in_table)
        return;
    end;
$$;


ALTER FUNCTION public.colpivot(out_table character varying, in_query character varying, key_cols character varying[], class_cols character varying[], value_e character varying, col_order character varying) OWNER TO phylomix;

--
-- Name: dist(text, text); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION dist(a1 text, a2 text) RETURNS integer
    LANGUAGE plpgsql IMMUTABLE
    AS $$
DECLARE b1 bit varying := to_bits(a1);
DECLARE b2 bit varying := to_bits(a2);
BEGIN
  RETURN length(replace((b1 # b2)::text, '0', ''));
END
$$;


ALTER FUNCTION public.dist(a1 text, a2 text) OWNER TO phylomix;

--
-- Name: dist_sample(integer); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION dist_sample(cutoff integer) RETURNS TABLE(distance integer, species1 text, annotation1 text, species2 text, annotation2 text)
    LANGUAGE sql
    AS $$
WITH a AS (
  SELECT a1.md5 AS a1_md5, a2.md5 AS a2_md5
       , dist(a1.lsh,a2.lsh) AS distance
  FROM nilsimsa a1
  INNER JOIN nilsimsa a2 ON a1.id < a2.id
  WHERE dist(a1.lsh,a2.lsh) <cutoff
  LIMIT 20
), b AS (
  SELECT distance
       , s1.scientific_name, s1.annotation
       , s2.scientific_name, s2.annotation
  FROM a
  LEFT JOIN seq_info s1 ON decode(a1_md5,'hex')=s1.md5
  LEFT JOIN seq_info s2 ON decode(a2_md5,'hex')=s2.md5
)
SELECT DISTINCT * FROM b;
END;

SELECT dist_sample(90);
SELECT dist_sample(70);
SELECT dist_sample(50);
SELECT dist_sample(30);
SELECT dist_sample(20);
SELECT dist_sample(10);
$$;


ALTER FUNCTION public.dist_sample(cutoff integer) OWNER TO phylomix;

--
-- Name: dist_sample(integer, integer); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION dist_sample(cutoff integer, lim integer) RETURNS TABLE(distance integer, species1 text, annotation1 text, species2 text, annotation2 text)
    LANGUAGE sql
    AS $$
WITH a AS (
  SELECT a1.md5 AS a1_md5, a2.md5 AS a2_md5
       , dist(a1.lsh,a2.lsh) AS distance
  FROM nilsimsa a1
  INNER JOIN nilsimsa a2 ON a1.id < a2.id
  WHERE dist(a1.lsh,a2.lsh) <cutoff
  LIMIT lim
), b AS (
  SELECT distance
       , s1.scientific_name, s1.annotation
       , s2.scientific_name, s2.annotation
  FROM a
  LEFT JOIN seq_info s1 ON decode(a1_md5,'hex')=s1.md5
  LEFT JOIN seq_info s2 ON decode(a2_md5,'hex')=s2.md5
)
SELECT DISTINCT * FROM b;
$$;


ALTER FUNCTION public.dist_sample(cutoff integer, lim integer) OWNER TO phylomix;

--
-- Name: dist_sample(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION dist_sample(lb integer, ub integer, lim integer) RETURNS TABLE(distance integer, species1 text, annotation1 text, species2 text, annotation2 text)
    LANGUAGE sql
    AS $$
WITH a AS (
  SELECT a1.md5 AS a1_md5, a2.md5 AS a2_md5
       , dist(a1.lsh,a2.lsh) AS distance
  FROM nilsimsa a1
  INNER JOIN nilsimsa a2 ON a1.id < a2.id
  WHERE dist(a1.lsh,a2.lsh) >=lb
    AND dist(a1.lsh,a2.lsh) < ub
  LIMIT lim
), b AS (
  SELECT distance
       , s1.scientific_name, s1.annotation
       , s2.scientific_name, s2.annotation
  FROM a
  LEFT JOIN seq_info s1 ON decode(a1_md5,'hex')=s1.md5
  LEFT JOIN seq_info s2 ON decode(a2_md5,'hex')=s2.md5
)
SELECT DISTINCT * FROM b;
$$;


ALTER FUNCTION public.dist_sample(lb integer, ub integer, lim integer) OWNER TO phylomix;

--
-- Name: functional_protein_family(integer, integer); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION functional_protein_family(ofs integer DEFAULT 0, lim integer DEFAULT 20) RETURNS TABLE(annotation text, species_list text, accession text[])
    LANGUAGE sql
    AS $$
WITH a AS (
SELECT annotation
     , species
     , accession
  FROM annotation_family
 LIMIT lim OFFSET ofs
)
SELECT annotation
     , string_agg(DISTINCT taxonomy, E'\n')
     , accession
  FROM a
  JOIN taxonomy ON scientific_name=ANY(species)
GROUP BY annotation, accession
;
$$;


ALTER FUNCTION public.functional_protein_family(ofs integer, lim integer) OWNER TO phylomix;

--
-- Name: get_aa_seq_spk(text); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION get_aa_seq_spk(id text) RETURNS integer
    LANGUAGE sql
    AS $$select spk from aa_seq where array[id] <@ names$$;


ALTER FUNCTION public.get_aa_seq_spk(id text) OWNER TO phylomix;

--
-- Name: ins_aa_seq(text, text); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION ins_aa_seq(name text, d text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    name1 TEXT;
    d1 TEXT;
    prev_names TEXT[];
    prev_spk   int;
BEGIN
    name1 := substring(name from '[\w.-]+');
    d1 := regexp_replace(UPPER(d), '[^\w.-]', '', 'g');
    SELECT spk, names INTO prev_spk, prev_names FROM aa_seq WHERE md5=md5(d1)::bytea;
    IF FOUND THEN
        IF NOT ARRAY[name1] <@ prev_names THEN
	    RAISE NOTICE '%, %, %', name1, prev_spk, prev_names;
            UPDATE aa_seq SET names=prev_names || name1 WHERE spk=prev_spk;
        END IF;
    ELSE
--        RAISE NOTICE '%, %, %', prev_spk, name1, prev_names;
        INSERT INTO aa_seq(names, data) SELECT ARRAY[name1], d1;
    END IF;
    RETURN 0;
END
$$;


ALTER FUNCTION public.ins_aa_seq(name text, d text) OWNER TO phylomix;

--
-- Name: ins_annot(); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION ins_annot() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ begin
insert into annotation(aa_seq_spk, note, note_md5) select get_aa_seq_spk(id), note, md5(note)::bytea;
end $$;


ALTER FUNCTION public.ins_annot() OWNER TO phylomix;

--
-- Name: ins_annotation(text, text); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION ins_annotation(id text, data text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    aaseq_spk INT;
BEGIN
    SELECT spk INTO aaseq_spk FROM aa_seq WHERE id = ANY(names);
    INSERT INTO annotation (aa_seq_spk, note)
         VALUES (aaseq_spk, data);
END
$$;


ALTER FUNCTION public.ins_annotation(id text, data text) OWNER TO phylomix;

--
-- Name: ins_blastclust(text); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION ins_blastclust(id_list text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    seqspk INT;

BEGIN
    SELECT spk INTO seqspk FROM aa_seq WHERE id_list && names; 
    if FOUND THEN
        INSERT INTO blastclust(seq_spk, seq_ids) VALUES (seqspk, id_list);
    ELSE
        RAISE NOTICE '% not found in sequence table', id_list;
    END IF;
    RETURN 0;
END
$$;


ALTER FUNCTION public.ins_blastclust(id_list text) OWNER TO phylomix;

--
-- Name: ins_blastclust(double precision, double precision, text[]); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION ins_blastclust(cov double precision, thr double precision, id_list text[]) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    thrspk INT;
    seqspks INT[];
BEGIN
    -- thr_spk
    INSERT INTO bclust_threshold(coverage, threshold)
        SELECT cov, thr
         WHERE NOT EXISTS (
	    SELECT 1 FROM bclust_threshold
	        WHERE coverage = cov AND threshold = thr);
    SELECT spk INTO thrspk FROM bclust_threshold
        WHERE coverage = cov AND threshold = thr;

    -- seqspks
    SELECT spk INTO seqspks FROM aa_seq WHERE id_list && names; 

    if FOUND THEN
        INSERT INTO blastclust(seq_spk, thr_spk, seq_spks)
                  VALUES (seqspk, cov, thr, seqspks);
    ELSE
        RAISE NOTICE '% not found in sequence table', id_list;
    END IF;
    RETURN 0;
END
$$;


ALTER FUNCTION public.ins_blastclust(cov double precision, thr double precision, id_list text[]) OWNER TO phylomix;

--
-- Name: ins_blastclust(double precision, double precision, text); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION ins_blastclust(cov double precision, thr double precision, id_list text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    thrspk INT;
    bcgspk INT;
    idlist TEXT[];
    id1 TEXT;
BEGIN
    -- get bclust_threshold.spk ..
    SELECT spk INTO thrspk FROM bclust_threshold
      WHERE coverage = cov AND threshold = thr;

    -- or insert if not exists
    IF NOT FOUND THEN
      INSERT INTO bclust_threshold(coverage, threshold)
        VALUES (cov, thr)
        RETURNING spk INTO thrspk;
    END IF;

    -- insert obtained thr_spk or notify
    INSERT INTO bclust_group(bclust_threshold_spk)
      VALUES (thrspk)
      RETURNING spk INTO bcgspk;

    -- id list
    idlist := STRING_TO_ARRAY(id_list, ',');

    -- seq_spks
    FOREACH id1 IN ARRAY idlist LOOP
      INSERT INTO blastclust(aa_seq_spk, bclust_group_spk)
        SELECT spk, bcgspk FROM aa_seq WHERE ARRAY[id1] <@ names;
    END LOOP;

    RETURN array_ndims(idlist);
END $$;


ALTER FUNCTION public.ins_blastclust(cov double precision, thr double precision, id_list text) OWNER TO phylomix;

--
-- Name: ins_blastp(text, text, double precision, integer, integer, integer, integer, integer, integer, integer, double precision, double precision); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION ins_blastp(qid text, sid text, idt double precision, len integer, mis integer, gap integer, qst integer, qen integer, sst integer, sen integer, evl double precision, bis double precision) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    q_spk INT;
    s_spk INT;
    
BEGIN
    SELECT spk INTO q_spk FROM aa_seq WHERE names @> ARRAY[qid];
    SELECT spk INTO s_spk FROM aa_seq WHERE names @> ARRAY[sid];
    RAISE NOTICE '% %  % %', q_spk, qid, s_spk, sid;
    INSERT INTO blastp(q_seqid,s_seqid,identity,len,mismatch,gap_opens,
                       q_start,q_end,s_start,s_end,evalue,bit_score)
	   VALUES (q_spk, s_spk, idt, len, mis, gap, qst, qen, sst, sen, evl, bis);
    RETURN 0;
END
$$;


ALTER FUNCTION public.ins_blastp(qid text, sid text, idt double precision, len integer, mis integer, gap integer, qst integer, qen integer, sst integer, sen integer, evl double precision, bis double precision) OWNER TO phylomix;

--
-- Name: popcnt(bytea); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION popcnt(b bytea) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE i INT;
DECLARE n INT :=0;
DECLARE len INT := octet_length(b) -1;
BEGIN
  FOR i in 0..len LOOP
    n := n + length(replace(get_byte(b,i)::bit(8)::text,'0',''));
  END LOOP;
  RETURN n;
END $$;


ALTER FUNCTION public.popcnt(b bytea) OWNER TO phylomix;

--
-- Name: popcnt2(bytea); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION popcnt2(b bytea) RETURNS integer
    LANGUAGE plpgsql
    AS $$
-- pit fall - offset 1 instead of 0
DECLARE BITS INT[] := '{
    0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8
}';
DECLARE i INT;
DECLARE n INT := 0;
DECLARE len INT := octet_length(b) - 1;
BEGIN
  FOR i IN 0..len LOOP
    n := n + BITS[ 1 + get_byte(b,i) ];
  END LOOP;
  RETURN n;
END $$;


ALTER FUNCTION public.popcnt2(b bytea) OWNER TO phylomix;

--
-- Name: quotient(integer, integer); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION quotient(a integer, b integer) RETURNS text
    LANGUAGE sql
    AS $$
select case b when 0 then '-' else to_char(a/b::float,'FM90.00') END;
$$;


ALTER FUNCTION public.quotient(a integer, b integer) OWNER TO phylomix;

--
-- Name: quotient(bigint, bigint); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION quotient(a bigint, b bigint) RETURNS text
    LANGUAGE sql
    AS $$
select case b when 0 then '-' else to_char(a/b::float,'FM90.00') END;
$$;


ALTER FUNCTION public.quotient(a bigint, b bigint) OWNER TO phylomix;

--
-- Name: sampling(integer, integer); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION sampling(lb integer, lim integer) RETURNS TABLE(distance integer, species1 text, annotation1 text, species2 text, annotation2 text)
    LANGUAGE plpgsql
    AS $$
DECLARE ub INT := lb+10;
BEGIN
  RAISE INFO '%-% lim %', lb, ub, lim;
  RETURN QUERY
  SELECT * FROM dist_sample(lb,ub,lim);
END
$$;


ALTER FUNCTION public.sampling(lb integer, lim integer) OWNER TO phylomix;

--
-- Name: set_md5(); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION set_md5() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
NEW.data := regexp_replace(UPPER(NEW.data),'[^\w._-]','','g'); NEW.md5 := md5(new.data);
return NEW;
end
$$;


ALTER FUNCTION public.set_md5() OWNER TO phylomix;

--
-- Name: FUNCTION set_md5(); Type: COMMENT; Schema: public; Owner: phylomix
--

COMMENT ON FUNCTION set_md5() IS 'Convert input data to upper case and Calcucate MD5 to set';


--
-- Name: sha1(text); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION sha1(text) RETURNS text
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$
select encode(digest($1, 'sha1'),'hex')
$_$;


ALTER FUNCTION public.sha1(text) OWNER TO phylomix;

--
-- Name: taxid(text); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION taxid(n text) RETURNS integer
    LANGUAGE sql
    AS $$
SELECT tax_id FROM tax_names WHERE name_txt=n
$$;


ALTER FUNCTION public.taxid(n text) OWNER TO phylomix;

--
-- Name: taxon_rank(text, text); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION taxon_rank(sp_name text, t_rank text) RETURNS TABLE(tax_id integer, taxon text)
    LANGUAGE sql
    AS $$
WITH RECURSIVE r AS (
  SELECT * FROM taxonomic_node_parent
    WHERE name_txt = sp_name
 UNION
  SELECT t.* FROM taxonomic_node_parent t, r
    WHERE t.tax_id = r.parent_tax_id
)
SELECT tax_id, name_txt
  FROM r WHERE rank = t_rank;
$$;


ALTER FUNCTION public.taxon_rank(sp_name text, t_rank text) OWNER TO phylomix;

--
-- Name: FUNCTION taxon_rank(sp_name text, t_rank text); Type: COMMENT; Schema: public; Owner: phylomix
--

COMMENT ON FUNCTION taxon_rank(sp_name text, t_rank text) IS 'Get phylum name from species name';


--
-- Name: to_bits(bytea); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION to_bits(b bytea) RETURNS bit varying
    LANGUAGE plpgsql
    AS $$
DECLARE n INT := octet_length(b) -1;
DECLARE i INT;
DECLARE ret bit varying = '';
BEGIN
  FOR i IN 0..n LOOP
    ret := ret || get_byte(b, i)::bit(8);
--    RAISE NOTICE 'b % i % n % ret % r %', b, i, n, ret, get_byte(b, i)::bit(8);
  END LOOP;
  RETURN ret;
END
$$;


ALTER FUNCTION public.to_bits(b bytea) OWNER TO phylomix;

--
-- Name: to_bits(integer); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION to_bits(integer) RETURNS bit varying
    LANGUAGE sql
    AS $_$
SELECT $1::bit(1);
$_$;


ALTER FUNCTION public.to_bits(integer) OWNER TO phylomix;

--
-- Name: to_bits(text); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION to_bits(text) RETURNS bit varying
    LANGUAGE sql
    AS $_$
SELECT to_bits( decode($1, 'base64') );
$_$;


ALTER FUNCTION public.to_bits(text) OWNER TO phylomix;

--
-- Name: urldecode_arr(text); Type: FUNCTION; Schema: public; Owner: phylomix
--

CREATE FUNCTION urldecode_arr(url text) RETURNS text
    LANGUAGE plpgsql IMMUTABLE STRICT
    AS $_$
DECLARE ret text;

BEGIN
 BEGIN

    WITH STR AS (
      SELECT
      
      -- array with all non encoded parts, prepend with '' when the string start is encoded
      case when $1 ~ '^%[0-9a-fA-F][0-9a-fA-F]' 
           then array[''] 
           end 
      || regexp_split_to_array ($1,'(%[0-9a-fA-F][0-9a-fA-F])+', 'i') plain,
      
      -- array with all encoded parts
      array(select (regexp_matches ($1,'((?:%[0-9a-fA-F][0-9a-fA-F])+)', 'gi'))[1]) encoded
    )
    SELECT  string_agg(plain[i] || coalesce( convert_from(decode(replace(encoded[i], '%',''), 'hex'), 'utf8'),''),'')
    FROM STR, 
      (SELECT  generate_series(1, array_upper(encoded,1)+2) i FROM STR)blah

    INTO ret;

  EXCEPTION WHEN OTHERS THEN  
    raise notice 'failed: %',url;
    return $1;
  END;   

  RETURN coalesce(ret,$1); -- when the string has no encoding;

END;

$_$;


ALTER FUNCTION public.urldecode_arr(url text) OWNER TO phylomix;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: species_dic; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE species_dic (
    scientific_name text NOT NULL,
    common_name text NOT NULL,
    publisher text NOT NULL,
    title text NOT NULL,
    revision text NOT NULL,
    id text NOT NULL
);


ALTER TABLE species_dic OWNER TO phylomix;

--
-- Name: annotation_family; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE annotation_family (
    annotation text,
    species_list text,
    n_phylum bigint,
    n_class bigint,
    n_order bigint,
    n_family bigint,
    n_species bigint,
    n_seq bigint,
    phyla phyla_enum[],
    classes text[],
    orders text[],
    families text[],
    accessions text[]
);


ALTER TABLE annotation_family OWNER TO phylomix;

--
-- Name: annotation_identity; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE annotation_identity (
    annotation text,
    species_list text,
    phylum bigint,
    class bigint,
    "order" bigint,
    family bigint,
    species bigint,
    seq bigint,
    accession text
);


ALTER TABLE annotation_identity OWNER TO phylomix;

--
-- Name: annotation_identity1; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE annotation_identity1 (
    annotation text,
    prefix text[],
    suffix text[],
    species_list text[],
    "#phyla" bigint,
    "#classes" bigint,
    "#orders" bigint,
    "#families" bigint,
    "#species" bigint,
    "#seqs" bigint,
    accession text[]
);


ALTER TABLE annotation_identity1 OWNER TO phylomix;

--
-- Name: annotation_predicted; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE annotation_predicted (
    annotation text
);


ALTER TABLE annotation_predicted OWNER TO phylomix;

--
-- Name: seq_info; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE seq_info (
    gi integer NOT NULL,
    accession text NOT NULL,
    annotation text NOT NULL,
    scientific_name text NOT NULL,
    md5 bytea NOT NULL
);


ALTER TABLE seq_info OWNER TO phylomix;

--
-- Name: seq_std_annotation; Type: VIEW; Schema: public; Owner: phylomix
--

CREATE VIEW seq_std_annotation AS
 SELECT seq_info.md5,
    seq_info.accession,
    seq_info.scientific_name,
    array_to_string(regexp_matches(seq_info.annotation, '^(?:PREDICTED: )?(.*?)(?:-like)?$'::text), ''::text) AS annotation,
    (seq_info.annotation ~ '^PREDICTED: '::text) AS predicted,
    (seq_info.annotation ~ '-like$'::text) AS _like
   FROM seq_info;


ALTER TABLE seq_std_annotation OWNER TO phylomix;

--
-- Name: annotation_spectra; Type: MATERIALIZED VIEW; Schema: public; Owner: phylomix
--

CREATE MATERIALIZED VIEW annotation_spectra AS
 WITH a AS (
         SELECT DISTINCT seq_std_annotation.annotation,
            seq_std_annotation.scientific_name,
            count(*) AS count
           FROM seq_std_annotation
          GROUP BY seq_std_annotation.annotation, seq_std_annotation.scientific_name
        )
 SELECT a.annotation,
    array_agg(DISTINCT ((a.scientific_name || ':'::text) || a.count)) AS species
   FROM a
  GROUP BY a.annotation
  WITH NO DATA;


ALTER TABLE annotation_spectra OWNER TO phylomix;

--
-- Name: annotation_spectra2; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE annotation_spectra2 (
    annotation text,
    prefix text[],
    suffix text[],
    species_list text,
    "#phyla" bigint,
    "#classes" bigint,
    "#orders" bigint,
    "#families" bigint,
    "#species" bigint,
    "#seqs" bigint,
    phylum phyla_enum[],
    class text[],
    "order" text[],
    family text[],
    accession text[]
);


ALTER TABLE annotation_spectra2 OWNER TO phylomix;

--
-- Name: annotation_suffix; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE annotation_suffix (
    array_to_string text
);


ALTER TABLE annotation_suffix OWNER TO phylomix;

--
-- Name: annotation_term_stat; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE annotation_term_stat (
    word text,
    ndoc integer,
    nentry integer
);


ALTER TABLE annotation_term_stat OWNER TO phylomix;

--
-- Name: blastclust_stats; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE blastclust_stats (
    scientific_name text NOT NULL,
    n_clusters integer,
    clst_max_size integer,
    clst_mean_size double precision,
    clst_stdev double precision,
    clst_log_mean double precision,
    clst_log_stdev double precision
);


ALTER TABLE blastclust_stats OWNER TO phylomix;

--
-- Name: gene_info; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE gene_info (
    tax_id integer,
    "GeneID" integer NOT NULL,
    "Symbol" text,
    "LocusTag" text,
    "Synonyms" text,
    "dbXrefs" text,
    chromosome text,
    map_location text,
    description text,
    type_of_gene text,
    symbol_from_nomenclature_authority text,
    full_name_from_nomenclature_authority text,
    nomenclature_status text,
    other_designations text,
    modification_date date
);


ALTER TABLE gene_info OWNER TO phylomix;

--
-- Name: tax_names; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE tax_names (
    tax_id integer NOT NULL,
    name_txt text NOT NULL,
    unique_name text,
    name_class text NOT NULL
);


ALTER TABLE tax_names OWNER TO phylomix;

--
-- Name: gene_view; Type: VIEW; Schema: public; Owner: phylomix
--

CREATE VIEW gene_view AS
 SELECT t."Species",
    gene_info.tax_id,
    gene_info."GeneID",
    gene_info."Symbol",
    gene_info."LocusTag",
    gene_info."Synonyms",
    gene_info."dbXrefs",
    gene_info.chromosome,
    gene_info.map_location,
    gene_info.description,
    gene_info.type_of_gene,
    gene_info.symbol_from_nomenclature_authority,
    gene_info.full_name_from_nomenclature_authority,
    gene_info.nomenclature_status,
    gene_info.other_designations,
    gene_info.modification_date
   FROM (( SELECT tax_names.tax_id,
            string_agg(tax_names.name_txt, '; '::text) AS "Species"
           FROM tax_names
          WHERE ((tax_names.name_class = 'scientific name'::text) OR (tax_names.name_class = 'genbank common name'::text))
          GROUP BY tax_names.tax_id) t
     JOIN gene_info USING (tax_id));


ALTER TABLE gene_view OWNER TO phylomix;

--
-- Name: genome_data_master; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE genome_data_master (
    taxon_id integer NOT NULL,
    scientific_name text NOT NULL,
    synonym text,
    common_name text,
    japanese_name text,
    phyla_num integer,
    phylum_text text NOT NULL,
    subphylum text,
    infraphylum text,
    class text,
    "order" text,
    family text NOT NULL,
    ebi text,
    ucsc text,
    ncbi text,
    comment text,
    phylum phyla_enum
);


ALTER TABLE genome_data_master OWNER TO phylomix;

--
-- Name: genome_master2; Type: VIEW; Schema: public; Owner: phylomix
--

CREATE VIEW genome_master2 AS
 SELECT DISTINCT taxon_id,
    scientific_name,
    common_name,
    japanese_name,
    phyid,
    subphylum,
    gdm.infraphylum,
    class,
    "order",
    family,
    gdm.synonym,
    gdm.ebi,
    gdm.ucsc,
    gdm.ncbi,
    gdm.comment
   FROM (genome_data_master gdm(taxon_id, scientific_name, synonym, common_name, japanese_name, phyla_num, phylum_text, subphylum, infraphylum, class, "order", family, ebi, ucsc, ncbi, comment, phyid)
     FULL JOIN intensity.genome_master gm(scientific_name, common_name, japanese_name, phylum_text, subphylum, class, "order", family, taxon_id, phyid) USING (taxon_id, scientific_name, phyid, subphylum, class, "order", family, common_name, japanese_name))
  GROUP BY taxon_id, scientific_name, common_name, japanese_name, phyid, subphylum, class, "order", family, gdm.synonym, gdm.infraphylum, gdm.ebi, gdm.ucsc, gdm.ncbi, gdm.comment
  ORDER BY phyid, class, "order", family, scientific_name;


ALTER TABLE genome_master2 OWNER TO phylomix;

--
-- Name: genome_master3; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE genome_master3 (
    taxon_id integer,
    scientific_name text,
    common_name text,
    japanese_name text,
    phylum phyla_enum,
    subphylum text,
    infraphylum text,
    class text,
    "order" text,
    family text,
    synonym text,
    ebi text,
    ucsc text,
    ncbi text,
    comment text
);


ALTER TABLE genome_master3 OWNER TO phylomix;

--
-- Name: genome_master3_dup; Type: VIEW; Schema: public; Owner: phylomix
--

CREATE VIEW genome_master3_dup AS
 SELECT DISTINCT genome_master3.scientific_name,
    array_agg(DISTINCT genome_master3.phylum) AS phylum,
    array_agg(DISTINCT genome_master3.subphylum) AS subphylum,
    array_agg(DISTINCT genome_master3.infraphylum) AS infraphylum,
    array_agg(DISTINCT genome_master3.class) AS class,
    array_agg(DISTINCT genome_master3."order") AS "order",
    array_agg(DISTINCT genome_master3.family) AS family
   FROM genome_master3
  GROUP BY genome_master3.scientific_name
 HAVING (count(*) > 1);


ALTER TABLE genome_master3_dup OWNER TO phylomix;

--
-- Name: genome_stats; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE genome_stats (
    scientific_name text NOT NULL,
    n_genes integer,
    len_mean double precision,
    len_stdev double precision,
    len_log_mean double precision,
    len_log_stdev double precision
);


ALTER TABLE genome_stats OWNER TO phylomix;

--
-- Name: scientific_name_to_tax_id; Type: VIEW; Schema: public; Owner: phylomix
--

CREATE VIEW scientific_name_to_tax_id AS
 SELECT tax_names.name_txt AS scientific_name,
    tax_names.unique_name,
    tax_names.tax_id
   FROM tax_names
  WHERE (tax_names.name_class = 'scientific name'::text);


ALTER TABLE scientific_name_to_tax_id OWNER TO phylomix;

--
-- Name: genome_master_stats; Type: VIEW; Schema: public; Owner: phylomix
--

CREATE VIEW genome_master_stats AS
 SELECT DISTINCT scientific_name,
    taxon_id,
    common_name,
    japanese_name,
    phylum,
    subphylum,
    class,
    "order",
    family,
    genome_data_master.synonym,
    genome_data_master.phyla_num AS phyid,
    genome_data_master.infraphylum,
    genome_data_master.ebi,
    genome_data_master.ucsc,
    genome_data_master.ncbi,
    genome_data_master.comment,
    genome_stats.n_genes,
    genome_stats.len_mean,
    genome_stats.len_stdev,
    genome_stats.len_log_mean,
    genome_stats.len_log_stdev,
    blastclust_stats.n_clusters,
    blastclust_stats.clst_max_size,
    blastclust_stats.clst_mean_size,
    blastclust_stats.clst_stdev,
    blastclust_stats.clst_log_mean,
    blastclust_stats.clst_log_stdev,
    scientific_name_to_tax_id.unique_name,
    scientific_name_to_tax_id.tax_id
   FROM ((((genome_data_master genome_data_master(taxon_id, scientific_name, synonym, common_name, japanese_name, phyla_num, phylum, subphylum, infraphylum, class, "order", family, ebi, ucsc, ncbi, comment, phylum_1)
     FULL JOIN intensity.genome_master gm(scientific_name, common_name, japanese_name, phylum, subphylum, class, "order", family, taxon_id, phylum_1) USING (taxon_id, scientific_name, common_name, japanese_name, phylum, subphylum, class, "order", family))
     FULL JOIN genome_stats USING (scientific_name))
     FULL JOIN blastclust_stats USING (scientific_name))
     LEFT JOIN scientific_name_to_tax_id USING (scientific_name))
  ORDER BY phylum, subphylum, class, "order", family, scientific_name, common_name;


ALTER TABLE genome_master_stats OWNER TO phylomix;

--
-- Name: genome_master_stats2; Type: VIEW; Schema: public; Owner: phylomix
--

CREATE VIEW genome_master_stats2 AS
 SELECT DISTINCT scientific_name,
    genome_master3.taxon_id,
    genome_master3.common_name,
    genome_master3.japanese_name,
    genome_master3.phylum,
    genome_master3.subphylum,
    genome_master3.infraphylum,
    genome_master3.class,
    genome_master3."order",
    genome_master3.family,
    genome_master3.synonym,
    genome_master3.ebi,
    genome_master3.ucsc,
    genome_master3.ncbi,
    genome_master3.comment,
    genome_stats.n_genes,
    genome_stats.len_mean,
    genome_stats.len_stdev,
    genome_stats.len_log_mean,
    genome_stats.len_log_stdev,
    blastclust_stats.n_clusters,
    blastclust_stats.clst_max_size,
    blastclust_stats.clst_mean_size,
    blastclust_stats.clst_stdev,
    blastclust_stats.clst_log_mean,
    blastclust_stats.clst_log_stdev,
    scientific_name_to_tax_id.unique_name,
    scientific_name_to_tax_id.tax_id
   FROM (((genome_master3
     FULL JOIN genome_stats USING (scientific_name))
     FULL JOIN blastclust_stats USING (scientific_name))
     LEFT JOIN scientific_name_to_tax_id USING (scientific_name))
  ORDER BY genome_master3.phylum, genome_master3.subphylum, genome_master3.class, genome_master3."order", genome_master3.family, scientific_name, genome_master3.common_name;


ALTER TABLE genome_master_stats2 OWNER TO phylomix;

--
-- Name: genome_table; Type: VIEW; Schema: public; Owner: phylomix
--

CREATE VIEW genome_table AS
 SELECT genome_master.phylum_text AS phylum,
    genome_master.subphylum,
    genome_master.class,
    genome_master."order",
    genome_master.family,
    scientific_name,
    genome_master.taxon_id,
    genome_master.common_name,
    genome_master.japanese_name,
    genome_stats.n_genes,
    genome_stats.len_mean,
    genome_stats.len_stdev,
    genome_stats.len_log_mean,
    genome_stats.len_log_stdev,
    blastclust_stats.n_clusters,
    blastclust_stats.clst_max_size,
    blastclust_stats.clst_mean_size,
    blastclust_stats.clst_stdev,
    blastclust_stats.clst_log_mean,
    blastclust_stats.clst_log_stdev
   FROM ((intensity.genome_master
     FULL JOIN genome_stats USING (scientific_name))
     FULL JOIN blastclust_stats USING (scientific_name))
  ORDER BY genome_master.phylum_text, genome_master.subphylum, genome_master.class, genome_master."order", genome_master.family, scientific_name, genome_master.common_name;


ALTER TABLE genome_table OWNER TO phylomix;

--
-- Name: identical_seq; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE identical_seq (
    md5 bytea,
    nilsimsa text,
    phylum_cnt bigint,
    class_cnt bigint,
    order_cnt bigint,
    family_cnt bigint,
    species_cnt bigint,
    sequence_cnt bigint,
    annotation text,
    species text,
    accession text
);


ALTER TABLE identical_seq OWNER TO phylomix;

--
-- Name: interpro; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE interpro (
    ipr_id text NOT NULL,
    annotation text,
    go_term text[]
);


ALTER TABLE interpro OWNER TO phylomix;

--
-- Name: interpro_seq; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE interpro_seq (
    ipr_id text,
    md5 bytea[]
);


ALTER TABLE interpro_seq OWNER TO phylomix;

--
-- Name: taxonomy; Type: VIEW; Schema: public; Owner: phylomix
--

CREATE VIEW taxonomy AS
 SELECT DISTINCT genome_master3.scientific_name,
    ((array_to_string(ARRAY[(genome_master3.phylum)::text, genome_master3.class, genome_master3."order", genome_master3.family, genome_master3.scientific_name], ';'::text) || ' # '::text) || genome_master3.common_name) AS taxonomy
   FROM genome_master3;


ALTER TABLE taxonomy OWNER TO phylomix;

--
-- Name: ipr_family; Type: MATERIALIZED VIEW; Schema: public; Owner: phylomix
--

CREATE MATERIALIZED VIEW ipr_family AS
 SELECT ip.ipr_id,
    ip.annotation,
    ip.go_term,
    array_agg(DISTINCT taxonomy.taxonomy) AS species_list,
    count(DISTINCT genome_master3.phylum) AS phylum,
    count(DISTINCT genome_master3.class) AS class,
    count(DISTINCT genome_master3."order") AS "order",
    count(DISTINCT genome_master3.family) AS family,
    count(DISTINCT s.scientific_name) AS species,
    count(DISTINCT s.md5) AS uniq_seq,
    count(s.md5) AS seq
   FROM ((((interpro ip
     LEFT JOIN interpro_seq i USING (ipr_id))
     LEFT JOIN seq_info s ON ((s.md5 = ANY (i.md5))))
     LEFT JOIN taxonomy USING (scientific_name))
     LEFT JOIN genome_master3 USING (scientific_name))
  GROUP BY ip.ipr_id, ip.annotation, ip.go_term
  WITH NO DATA;


ALTER TABLE ipr_family OWNER TO phylomix;

--
-- Name: md5_interpro; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE md5_interpro (
    md5 bytea,
    ipr_id text
);


ALTER TABLE md5_interpro OWNER TO phylomix;

--
-- Name: ipr_spectra; Type: MATERIALIZED VIEW; Schema: public; Owner: phylomix
--

CREATE MATERIALIZED VIEW ipr_spectra AS
 SELECT md5_interpro.ipr_id,
    s.scientific_name,
    count(*) AS n_seq,
    array_agg(s.accession) AS accession
   FROM (seq_info s
     LEFT JOIN md5_interpro USING (md5))
  GROUP BY md5_interpro.ipr_id, s.scientific_name
  WITH NO DATA;


ALTER TABLE ipr_spectra OWNER TO phylomix;

--
-- Name: ipr_spectra_sp; Type: MATERIALIZED VIEW; Schema: public; Owner: phylomix
--

CREATE MATERIALIZED VIEW ipr_spectra_sp AS
 SELECT s.md5,
    s.accession,
    s.scientific_name,
    md5_interpro.ipr_id,
    count(*) AS count
   FROM (seq_info s
     LEFT JOIN md5_interpro USING (md5))
  GROUP BY s.md5, s.accession, s.scientific_name, md5_interpro.ipr_id
  WITH NO DATA;


ALTER TABLE ipr_spectra_sp OWNER TO phylomix;

--
-- Name: ipr_spectra_summary; Type: MATERIALIZED VIEW; Schema: public; Owner: phylomix
--

CREATE MATERIALIZED VIEW ipr_spectra_summary AS
 SELECT ip.ipr_id,
    ip.annotation,
    ip.go_term,
    array_agg(DISTINCT taxonomy.taxonomy) AS species_list,
    count(DISTINCT genome_master3.phylum) AS phylum,
    count(DISTINCT genome_master3.class) AS class,
    count(DISTINCT genome_master3."order") AS "order",
    count(DISTINCT genome_master3.family) AS family,
    count(DISTINCT s.scientific_name) AS species,
    count(DISTINCT s.md5) AS uniq_seq,
    count(DISTINCT s.accession) AS seq
   FROM ((((interpro ip
     LEFT JOIN interpro_seq i USING (ipr_id))
     LEFT JOIN seq_info s ON ((s.md5 = ANY (i.md5))))
     LEFT JOIN taxonomy USING (scientific_name))
     LEFT JOIN genome_master3 USING (scientific_name))
  GROUP BY ip.ipr_id, ip.annotation, ip.go_term
  WITH NO DATA;


ALTER TABLE ipr_spectra_summary OWNER TO phylomix;

--
-- Name: nilsimsa; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE nilsimsa (
    md5 text NOT NULL,
    lsh text,
    id integer NOT NULL
);


ALTER TABLE nilsimsa OWNER TO phylomix;

--
-- Name: nilsimsa_family; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE nilsimsa_family (
    lsh text,
    md5 bytea[]
);


ALTER TABLE nilsimsa_family OWNER TO phylomix;

--
-- Name: nilsimsa_id_seq; Type: SEQUENCE; Schema: public; Owner: phylomix
--

CREATE SEQUENCE nilsimsa_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE nilsimsa_id_seq OWNER TO phylomix;

--
-- Name: nilsimsa_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: phylomix
--

ALTER SEQUENCE nilsimsa_id_seq OWNED BY nilsimsa.id;


--
-- Name: phylum_id; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE phylum_id (
    phyid integer,
    phylum text
);


ALTER TABLE phylum_id OWNER TO phylomix;

--
-- Name: ref_mhlw; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE ref_mhlw (
    id text,
    scientific_name text NOT NULL,
    common_name text NOT NULL,
    japanese_name text NOT NULL
);


ALTER TABLE ref_mhlw OWNER TO phylomix;

--
-- Name: scientific_name_to_taxon_id; Type: VIEW; Schema: public; Owner: phylomix
--

CREATE VIEW scientific_name_to_taxon_id AS
 SELECT tax_names.name_txt AS scientific_name,
    tax_names.unique_name,
    tax_names.tax_id AS taxon_id
   FROM tax_names
  WHERE (tax_names.name_class = 'scientific name'::text);


ALTER TABLE scientific_name_to_taxon_id OWNER TO phylomix;

--
-- Name: seq_info1; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE seq_info1 (
    gi integer,
    accession text,
    scientific_name text,
    md5 bytea,
    annotation text,
    prefix text,
    suffix text
);


ALTER TABLE seq_info1 OWNER TO phylomix;

--
-- Name: seq_md5; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE seq_md5 (
    md5 bytea NOT NULL,
    seq text
);


ALTER TABLE seq_md5 OWNER TO phylomix;

--
-- Name: seq_info1_fasta_view; Type: VIEW; Schema: public; Owner: phylomix
--

CREATE VIEW seq_info1_fasta_view AS
 SELECT seq_info1.gi,
    seq_info1.accession,
    seq_info1.scientific_name,
    seq_info1.prefix,
    seq_info1.suffix,
    seq_info1.md5,
    seq_md5.seq
   FROM (seq_info1
     JOIN seq_md5 USING (md5));


ALTER TABLE seq_info1_fasta_view OWNER TO phylomix;

--
-- Name: seq_info2; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE seq_info2 (
    gi integer,
    accession text,
    scientific_name text,
    md5 bytea,
    annotation text,
    prefix text,
    suffix text
);


ALTER TABLE seq_info2 OWNER TO phylomix;

--
-- Name: seq_info2_identical_seq_with_different_annotation; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE seq_info2_identical_seq_with_different_annotation (
    md5 bytea,
    string_agg text,
    species text
);


ALTER TABLE seq_info2_identical_seq_with_different_annotation OWNER TO phylomix;

--
-- Name: seq_info_chl; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE seq_info_chl (
)
INHERITS (seq_info);


ALTER TABLE seq_info_chl OWNER TO phylomix;

--
-- Name: seq_info_mt; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE seq_info_mt (
)
INHERITS (seq_info);


ALTER TABLE seq_info_mt OWNER TO phylomix;

--
-- Name: seq_info_nuc; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE seq_info_nuc (
)
INHERITS (seq_info);


ALTER TABLE seq_info_nuc OWNER TO phylomix;

--
-- Name: seq_interpro; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE seq_interpro (
    md5 bytea NOT NULL,
    ipr_id text[]
);


ALTER TABLE seq_interpro OWNER TO phylomix;

--
-- Name: seq_md5_go; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE seq_md5_go (
    seq_md5 bytea NOT NULL,
    goterm_id text
);


ALTER TABLE seq_md5_go OWNER TO phylomix;

--
-- Name: taxonomic_nodes; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE taxonomic_nodes (
    tax_id integer NOT NULL,
    parent_tax_id integer,
    rank text
);


ALTER TABLE taxonomic_nodes OWNER TO phylomix;

--
-- Name: taxonomic_node_parent; Type: VIEW; Schema: public; Owner: phylomix
--

CREATE VIEW taxonomic_node_parent AS
 SELECT tax_names.tax_id,
    tax_names.name_txt,
    tax_names.unique_name,
    tax_names.name_class,
    taxonomic_nodes.parent_tax_id,
    taxonomic_nodes.rank
   FROM (tax_names
     JOIN taxonomic_nodes USING (tax_id))
  WHERE (tax_names.name_class = 'scientific name'::text);


ALTER TABLE taxonomic_node_parent OWNER TO phylomix;

--
-- Name: test; Type: TABLE; Schema: public; Owner: phylomix
--

CREATE TABLE test (
    id integer NOT NULL,
    a bytea
);


ALTER TABLE test OWNER TO phylomix;

--
-- Name: test_id_seq; Type: SEQUENCE; Schema: public; Owner: phylomix
--

CREATE SEQUENCE test_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE test_id_seq OWNER TO phylomix;

--
-- Name: test_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: phylomix
--

ALTER SEQUENCE test_id_seq OWNED BY test.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: phylomix
--

ALTER TABLE ONLY nilsimsa ALTER COLUMN id SET DEFAULT nextval('nilsimsa_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: phylomix
--

ALTER TABLE ONLY test ALTER COLUMN id SET DEFAULT nextval('test_id_seq'::regclass);


--
-- Name: blastclust_stats_pkey; Type: CONSTRAINT; Schema: public; Owner: phylomix
--

ALTER TABLE ONLY blastclust_stats
    ADD CONSTRAINT blastclust_stats_pkey PRIMARY KEY (scientific_name);


--
-- Name: gene_info_pkey; Type: CONSTRAINT; Schema: public; Owner: phylomix
--

ALTER TABLE ONLY gene_info
    ADD CONSTRAINT gene_info_pkey PRIMARY KEY ("GeneID");


--
-- Name: genome_stats_pkey; Type: CONSTRAINT; Schema: public; Owner: phylomix
--

ALTER TABLE ONLY genome_stats
    ADD CONSTRAINT genome_stats_pkey PRIMARY KEY (scientific_name);


--
-- Name: interpro_pkey; Type: CONSTRAINT; Schema: public; Owner: phylomix
--

ALTER TABLE ONLY interpro
    ADD CONSTRAINT interpro_pkey PRIMARY KEY (ipr_id);


--
-- Name: nilsimsa_pkey; Type: CONSTRAINT; Schema: public; Owner: phylomix
--

ALTER TABLE ONLY nilsimsa
    ADD CONSTRAINT nilsimsa_pkey PRIMARY KEY (md5);


--
-- Name: ref_mhlw_scientific_name_key; Type: CONSTRAINT; Schema: public; Owner: phylomix
--

ALTER TABLE ONLY ref_mhlw
    ADD CONSTRAINT ref_mhlw_scientific_name_key UNIQUE (scientific_name);


--
-- Name: seq_info_pkey; Type: CONSTRAINT; Schema: public; Owner: phylomix
--

ALTER TABLE ONLY seq_info
    ADD CONSTRAINT seq_info_pkey PRIMARY KEY (gi);


--
-- Name: seq_interpro_pkey; Type: CONSTRAINT; Schema: public; Owner: phylomix
--

ALTER TABLE ONLY seq_interpro
    ADD CONSTRAINT seq_interpro_pkey PRIMARY KEY (md5);


--
-- Name: seq_md5_go_pkey; Type: CONSTRAINT; Schema: public; Owner: phylomix
--

ALTER TABLE ONLY seq_md5_go
    ADD CONSTRAINT seq_md5_go_pkey PRIMARY KEY (seq_md5);


--
-- Name: seq_md5_pkey; Type: CONSTRAINT; Schema: public; Owner: phylomix
--

ALTER TABLE ONLY seq_md5
    ADD CONSTRAINT seq_md5_pkey PRIMARY KEY (md5);


--
-- Name: taxonomic_nodes_pkey; Type: CONSTRAINT; Schema: public; Owner: phylomix
--

ALTER TABLE ONLY taxonomic_nodes
    ADD CONSTRAINT taxonomic_nodes_pkey PRIMARY KEY (tax_id);


--
-- Name: annotation_spectra2_#classes_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX "annotation_spectra2_#classes_idx" ON annotation_spectra2 USING btree ("#classes");


--
-- Name: annotation_spectra2_#phyla_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX "annotation_spectra2_#phyla_idx" ON annotation_spectra2 USING btree ("#phyla");


--
-- Name: annotation_spectra2_annotation_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX annotation_spectra2_annotation_idx ON annotation_spectra2 USING btree (annotation);


--
-- Name: gene_info_Symbol_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX "gene_info_Symbol_idx" ON gene_info USING btree ("Symbol");


--
-- Name: gene_info_Synonyms_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX "gene_info_Synonyms_idx" ON gene_info USING btree ("Synonyms") WHERE ("Synonyms" IS NOT NULL);


--
-- Name: gene_info_dbXrefs_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX "gene_info_dbXrefs_idx" ON gene_info USING btree ("dbXrefs") WHERE ("dbXrefs" IS NOT NULL);


--
-- Name: gene_info_to_tsvector_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX gene_info_to_tsvector_idx ON gene_info USING gin (to_tsvector('english'::regconfig, ((description || ' '::text) || full_name_from_nomenclature_authority)));


--
-- Name: gene_info_to_tsvector_idx1; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX gene_info_to_tsvector_idx1 ON gene_info USING gin (to_tsvector('english'::regconfig, (((("Symbol" || ' '::text) || "Synonyms") || ' '::text) || symbol_from_nomenclature_authority)));


--
-- Name: gene_info_to_tsvector_idx2; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX gene_info_to_tsvector_idx2 ON gene_info USING gin (to_tsvector('english'::regconfig, lower(((description || ' '::text) || full_name_from_nomenclature_authority))));


--
-- Name: gene_info_to_tsvector_idx3; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX gene_info_to_tsvector_idx3 ON gene_info USING gin (to_tsvector('english'::regconfig, lower((((("Symbol" || ' '::text) || "Synonyms") || ' '::text) || symbol_from_nomenclature_authority))));


--
-- Name: genome_master3_scientific_name_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX genome_master3_scientific_name_idx ON genome_master3 USING btree (scientific_name);


--
-- Name: interpro_annotation_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX interpro_annotation_idx ON interpro USING btree (annotation);


--
-- Name: interpro_go_term_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX interpro_go_term_idx ON interpro USING gin (go_term);


--
-- Name: interpro_seq_md5_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX interpro_seq_md5_idx ON interpro_seq USING gin (md5);


--
-- Name: interpro_to_tsvector_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX interpro_to_tsvector_idx ON interpro USING gin (to_tsvector('english'::regconfig, annotation));


--
-- Name: md5_interpro_ipr_id_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX md5_interpro_ipr_id_idx ON md5_interpro USING btree (ipr_id);


--
-- Name: md5_interpro_md5_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX md5_interpro_md5_idx ON md5_interpro USING btree (md5);


--
-- Name: nilsimsa_lsh_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX nilsimsa_lsh_idx ON nilsimsa USING btree (lsh);


--
-- Name: seq_info1_annotation_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX seq_info1_annotation_idx ON seq_info1 USING btree (annotation);


--
-- Name: seq_info1_annotation_idx1; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX seq_info1_annotation_idx1 ON seq_info1 USING btree (annotation);


--
-- Name: seq_info1_md5_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX seq_info1_md5_idx ON seq_info1 USING btree (md5);


--
-- Name: seq_info1_md5_idx1; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX seq_info1_md5_idx1 ON seq_info1 USING btree (md5);


--
-- Name: seq_info1_scientific_name_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX seq_info1_scientific_name_idx ON seq_info1 USING btree (scientific_name);


--
-- Name: seq_info1_scientific_name_idx1; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX seq_info1_scientific_name_idx1 ON seq_info1 USING btree (scientific_name);


--
-- Name: seq_info1_to_tsvector_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX seq_info1_to_tsvector_idx ON seq_info1 USING gin (to_tsvector('english'::regconfig, annotation));


--
-- Name: seq_info2_annotation_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX seq_info2_annotation_idx ON seq_info2 USING btree (annotation);


--
-- Name: seq_info2_md5_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX seq_info2_md5_idx ON seq_info2 USING btree (md5);


--
-- Name: seq_info2_scientific_name_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX seq_info2_scientific_name_idx ON seq_info2 USING btree (scientific_name);


--
-- Name: seq_info2_to_tsvector_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX seq_info2_to_tsvector_idx ON seq_info2 USING gin (to_tsvector('english'::regconfig, annotation));


--
-- Name: seq_info_accession_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE UNIQUE INDEX seq_info_accession_idx ON seq_info USING btree (accession);


--
-- Name: seq_info_annotation_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX seq_info_annotation_idx ON seq_info USING btree (annotation);


--
-- Name: seq_info_chl_accession_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE UNIQUE INDEX seq_info_chl_accession_idx ON seq_info_chl USING btree (accession);


--
-- Name: seq_info_chl_md5_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX seq_info_chl_md5_idx ON seq_info_chl USING btree (md5);


--
-- Name: seq_info_chl_scientific_name_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX seq_info_chl_scientific_name_idx ON seq_info_chl USING btree (scientific_name);


--
-- Name: seq_info_chl_to_tsvector_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX seq_info_chl_to_tsvector_idx ON seq_info_chl USING gin (to_tsvector('english'::regconfig, ((scientific_name || ' '::text) || annotation)));


--
-- Name: seq_info_md5_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX seq_info_md5_idx ON seq_info USING btree (md5);


--
-- Name: seq_info_mt_accession_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE UNIQUE INDEX seq_info_mt_accession_idx ON seq_info_mt USING btree (accession);


--
-- Name: seq_info_mt_md5_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX seq_info_mt_md5_idx ON seq_info_mt USING btree (md5);


--
-- Name: seq_info_mt_scientific_name_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX seq_info_mt_scientific_name_idx ON seq_info_mt USING btree (scientific_name);


--
-- Name: seq_info_mt_to_tsvector_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX seq_info_mt_to_tsvector_idx ON seq_info_mt USING gin (to_tsvector('english'::regconfig, ((scientific_name || ' '::text) || annotation)));


--
-- Name: seq_info_scientific_name_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX seq_info_scientific_name_idx ON seq_info USING btree (scientific_name);


--
-- Name: seq_info_to_tsvector_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX seq_info_to_tsvector_idx ON seq_info USING gin (to_tsvector('english'::regconfig, ((scientific_name || ' '::text) || annotation)));


--
-- Name: seq_interpro_ipr_id_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX seq_interpro_ipr_id_idx ON seq_interpro USING gin (ipr_id);


--
-- Name: tax_names_name_class_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX tax_names_name_class_idx ON tax_names USING btree (name_class);


--
-- Name: tax_names_name_txt_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX tax_names_name_txt_idx ON tax_names USING btree (name_txt) WHERE (name_class = 'authority'::text);


--
-- Name: tax_names_name_txt_idx1; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX tax_names_name_txt_idx1 ON tax_names USING btree (name_txt) WHERE (name_class = 'scientific name'::text);


--
-- Name: tax_names_tax_id_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX tax_names_tax_id_idx ON tax_names USING btree (tax_id);


--
-- Name: tax_names_tax_id_idx1; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX tax_names_tax_id_idx1 ON tax_names USING btree (tax_id) WHERE (name_class = 'scientific name'::text);


--
-- Name: tax_names_unique_name_idx; Type: INDEX; Schema: public; Owner: phylomix
--

CREATE INDEX tax_names_unique_name_idx ON tax_names USING btree (unique_name);


--
-- Name: taxonomic_nodes_parent_tax_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: phylomix
--

ALTER TABLE ONLY taxonomic_nodes
    ADD CONSTRAINT taxonomic_nodes_parent_tax_id_fkey FOREIGN KEY (parent_tax_id) REFERENCES taxonomic_nodes(tax_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

