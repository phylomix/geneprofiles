
psql -U phylomix <<'EOT'
BEGIN;
SELECT drop_indice('seq_info1');
TRUNCATE seq_info1;
INSERT INTO seq_info1
WITH a AS (
  SELECT *, regexp_matches(annotation,
        '^((?:PREDICTED: )?(?:LOW QUALITY PROTEIN:|putative|probable|uncharacterized protein|protein )?)?'
	||'(\S+?)'
	||'((?:-like)?,?(?: (?:partial|(?:X )?isoform ?(?:[A-Za-z]|X?\d+)?|homolog(?: [A-Z|\d])?))*|family [ \w]*member[ \d]*| precursor)*(\(.*\))*$') AS x
    FROM seq_info
)
SELECT gi,accession,scientific_name,md5
     , trim(',- ' from x[2]) AS annotation
     , trim(', ' from x[1]) AS prefix
     , trim(', ' from x[3]) AS suffix
     , trim(', ' from x[4]) AS note
  FROM a
;
COMMENT ON TABLE seq_info1 IS 'sequence info with standardized annotation';

CREATE OR REPLACE VIEW seq_info1_fasta_view AS
SELECT gi, accession, scientific_name, prefix, suffix, md5, seq
FROM seq_info1
JOIN seq_md5 USING (md5)
;
CREATE INDEX ON seq_info1 (scientific_name);
CREATE INDEX ON seq_info1 (md5);
CREATE INDEX ON seq_info1 (lower(annotation));
CREATE INDEX ON seq_info1 USING gin(to_tsvector('English', annotation));
COMMIT;

/*
REFRESH MATERIALIZED VIEW md5_annotation_diffs;
BEGIN;
SELECT drop_indice('annotation_identity1');
REFRESH MATERIALIZED VIEW annotation_identity1;

*/

-- 
DROP MATERIALIZED VIEW IF EXISTS annotation_identity1;
CREATE MATERIALIZED VIEW annotation_identity1 AS
SELECT lower(annotation) AS annotation0
     , annotation AS annotation
     , array_agg(DISTINCT prefix) as prefix
     , array_agg(DISTINCT suffix) as suffix
     , array_agg(DISTINCT taxonomy) as species_list
     , count(DISTINCT phylum) AS "#phyla"
     , count(DISTINCT class) AS "#classes"
     , count(DISTINCT "order") AS "#orders"
     , count(DISTINCT family) AS "#families"
     , count(DISTINCT scientific_name) AS "#species"
     , count(DISTINCT md5) AS "#seqs"
     , array_agg(DISTINCT md5) AS "md5"
     , array_agg(DISTINCT accession) AS "accession"
  FROM seq_info1
  JOIN taxonomy USING (scientific_name)
  JOIN genome_master3 USING (scientific_name)
 GROUP BY annotation
;
COMMENT ON TABLE annotation_identity1 IS 'identical annotation table master';
CREATE INDEX ON annotation_identity1 (annotation);
CREATE INDEX ON annotation_identity1 USING GIN(species_list);

COMMIT;
EOT

