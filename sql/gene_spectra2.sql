
psql -U phylomix <<'EOT'
/*
--
DROP MATERIALIZED VIEW IF EXISTS class_spectra2;
CREATE MATERIALIZED VIEW class_spectra2 AS
WITH a AS (
 SELECT annotation
      , array_agg(DISTINCT phylum) AS phyla
      , array_agg(DISTINCT class) AS classes
   FROM seq_info1
   JOIN genome_master3 USING (scientific_name)
  GROUP BY annotation
)
SELECT phyla
     , classes
     , array_agg(DISTINCT annotation) AS gene_function
  FROM a
 GROUP BY phyla, classes
 ORDER BY array_length(classes,1) DESC
;

--
DROP MATERIALIZED VIEW IF EXISTS order_spectra2;
CREATE MATERIALIZED VIEW order_spectra2 AS
WITH a AS (
 SELECT annotation
      , array_agg(DISTINCT phylum) AS phyla
      , array_agg(DISTINCT class) AS classes
      , array_agg(DISTINCT "order") AS orders
   FROM seq_info1
   JOIN genome_master3 USING (scientific_name)
  GROUP BY annotation
)
SELECT phyla
     , classes
     , orders
     , array_agg(DISTINCT annotation) AS gene_function
  FROM a
 GROUP BY phyla, classes, orders
 ORDER BY array_length(orders,1) DESC
;

--
DROP MATERIALIZED VIEW IF EXISTS family_spectra2;
CREATE MATERIALIZED VIEW family_spectra2 AS
WITH a AS (
 SELECT annotation
      , array_agg(DISTINCT phylum) AS phyla
      , array_agg(DISTINCT class) AS classes
      , array_agg(DISTINCT "order") AS orders
      , array_agg(DISTINCT family) AS families
   FROM seq_info1
   JOIN genome_master3 USING (scientific_name)
  GROUP BY annotation
)
SELECT phyla
     , classes
     , orders
     , families
     , array_agg(DISTINCT annotation) AS gene_function
  FROM a
 GROUP BY phyla, classes, orders, families
 ORDER BY array_length(families,1) DESC
;
*/
-- 
DROP MATERIALIZED VIEW IF EXISTS species_spectra2;
CREATE MATERIALIZED VIEW species_spectra2 AS
WITH a AS (
 SELECT annotation
      , array_agg(DISTINCT phylum) AS phyla
      , array_agg(DISTINCT class) AS classes
      , array_agg(DISTINCT "order") AS orders
      , array_agg(DISTINCT family) AS families
      , array_agg(DISTINCT scientific_name ||
                  CASE WHEN common_name IS NOT NULL 
		       THEN ' (' || common_name || ')' END ) AS species
   FROM seq_info1
   JOIN genome_master3 USING (scientific_name)
  GROUP BY annotation
)
SELECT phyla
     , classes
     , orders
     , families
     , species
     , array_agg(DISTINCT annotation) AS gene_function
  FROM a
 GROUP BY phyla, classes, orders, families, species
 ORDER BY array_length(species,1) DESC
;

EOT