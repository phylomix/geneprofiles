
psql -U phylomix <<'EOT'
/*
BEGIN;
DROP TABLE IF EXISTS seq_info1 CASCADE;
CREATE TABLE seq_info1 AS
WITH a AS (
  SELECT *, regexp_matches(annotation,
        '(PREDICTED: LOW QUALITY PROTEIN:|PREDICTED: putative|PREDICTED: protein |PREDICTED:|probable)?'
	||'(.*)'
	||'(-like, partial|-like isoform X?\d+|-like|isoform X?\d+|isoform [a-z]|homolog)$') AS x
    FROM seq_info
)
SELECT gi,accession,scientific_name,md5
     , trim(',- ' from x[2]) AS annotation
     , trim(', ' from x[1]) AS prefix
     , trim(', ' from x[3]) AS suffix
  FROM a
;
COMMENT ON TABLE seq_info1 IS 'sequence info with standardized annotation';

CREATE OR REPLACE VIEW seq_info1_fasta_view AS
SELECT gi, accession, scientific_name, prefix, suffix, md5, seq
FROM seq_info1
JOIN seq_md5 USING (md5)
;
CREATE INDEX ON seq_info1 (scientific_name);
CREATE INDEX ON seq_info1 (md5);
CREATE INDEX ON seq_info1 (annotation);
CREATE INDEX ON seq_info1 USING gin(to_tsvector('English', annotation));

-- 
DROP TABLE IF EXISTS annotation_identity1;
DROP MATERIALIZED VIEW  IF EXISTS annotation_identity1;
CREATE MATERIALIZED VIEW annotation_identity1 AS
SELECT annotation
     , array_agg(DISTINCT prefix) as prefix
     , array_agg(DISTINCT suffix) as suffix
     , array_agg(DISTINCT taxonomy) as species_list
     , count(DISTINCT phylum) AS "#phyla"
     , count(DISTINCT class) AS "#classes"
     , count(DISTINCT "order") AS "#orders"
     , count(DISTINCT family) AS "#families"
     , count(DISTINCT scientific_name) AS "#species"
     , count(DISTINCT md5) AS "#seqs"
     , array_agg(DISTINCT accession) AS "accession"
  FROM seq_info1
  JOIN taxonomy USING (scientific_name)
  JOIN genome_master3 USING (scientific_name)
 GROUP BY annotation
;
COMMENT ON TABLE annotation_identity1 IS 'identical annotation table master';

CREATE INDEX ON annotation_identity1 (annotation);
CREATE INDEX ON annotation_identity1 (species_list);
*/


--CREATE INDEX ON seq_info1 (lower(annotation));
/*
CREATE AGGREGATE array_agg(a TEXT[])
( sfunc = array_cat,  stype = TEXT[] );
*/

DROP MATERIALIZED VIEW IF EXISTS phylum_spectra;
CREATE MATERIALIZED VIEW phylum_spectra AS
WITH phylum_list AS (
 SELECT distinct phylum, spk FROM phylum_type
), a AS (
 SELECT lower(annotation) as a0
      , array_agg(DISTINCT annotation) as a1
      , array_agg(DISTINCT phylum) AS phyla
   FROM seq_info1
   JOIN genome_master3 USING (scientific_name)
  GROUP BY lower(annotation)
)
SELECT phyla
     , array(SELECT CASE WHEN n IS NULL THEN '--'
                         ELSE left(phylum::text,2)
                    END FROM phylum_list LEFT JOIN unnest(phyla) n ON n::TEXT=phylum
		        ORDER BY spk)
       AS phylum_set
     , array_agg(DISTINCT a0) AS gene_function
     , array_agg(DISTINCT a1) AS annotation
  FROM a
 GROUP BY phyla, phylum_set
 ORDER BY array_length(phyla,1) DESC
;
select distinct phylum_set from phylum_spectra limit 10;

--
DROP MATERIALIZED VIEW IF EXISTS class_spectra;
CREATE MATERIALIZED VIEW class_spectra AS
WITH a AS (
 SELECT lower(annotation) as a0
      , array_agg(DISTINCT annotation) as a1
      , phylum
      , array_agg(DISTINCT class) AS classes
   FROM seq_info1
   JOIN genome_master3 USING (scientific_name)
  GROUP BY annotation, phylum
)
SELECT phylum
     , classes
     , array_agg(DISTINCT a0) AS gene_function
     , array_agg(DISTINCT a1) AS annotation
  FROM a
 GROUP BY phylum, classes
 ORDER BY array_length(classes,1) DESC
;

--
DROP MATERIALIZED VIEW IF EXISTS order_spectra;
CREATE MATERIALIZED VIEW order_spectra AS
WITH a AS (
 SELECT lower(annotation) as a0
      , array_agg(DISTINCT annotation) as a1
      , phylum
      , class
      , array_agg(DISTINCT "order") AS orders
   FROM seq_info1
   JOIN genome_master3 USING (scientific_name)
  GROUP BY annotation, phylum, class
)
SELECT phylum
     , class
     , orders
     , array_agg(DISTINCT a0) AS gene_function
     , array_agg(DISTINCT a1) AS annotation
  FROM a
 GROUP BY phylum, class, orders
 ORDER BY array_length(orders,1) DESC
;

--
DROP MATERIALIZED VIEW IF EXISTS family_spectra;
CREATE MATERIALIZED VIEW family_spectra AS
WITH a AS (
 SELECT lower(annotation) as a0
      , array_agg(DISTINCT annotation) as a1
      , phylum
      , class
      , "order"
      , array_agg(DISTINCT family) AS families
   FROM seq_info1
   JOIN genome_master3 USING (scientific_name)
  GROUP BY annotation, phylum, class, "order"
)
SELECT phylum
     , class
     , "order"
     , families
     , array_agg(DISTINCT a0) AS gene_function
     , array_agg(DISTINCT a1) AS annotation
  FROM a
 GROUP BY phylum, class, "order", families
 ORDER BY array_length(families,1) DESC
;

--
DROP MATERIALIZED VIEW IF EXISTS species_spectra;
CREATE MATERIALIZED VIEW species_spectra AS
WITH a AS (
 SELECT lower(annotation) as a0
      , array_agg(DISTINCT annotation) as a1
      , phylum
      , class
      , "order"
      , family
      , array_agg(DISTINCT scientific_name ||
                  CASE WHEN common_name IS NOT NULL 
		       THEN ' (' || common_name || ')' END ) AS species
   FROM seq_info1
   JOIN genome_master3 USING (scientific_name)
  GROUP BY annotation, phylum, class, "order", family
)
SELECT phylum
     , class
     , "order"
     , family
     , species
     , array_agg(DISTINCT a0) AS gene_function
     , array_agg(DISTINCT a1) AS annotation
  FROM a
 GROUP BY phylum, class, "order", family, species
 ORDER BY array_length(species,1) DESC
;

COMMIT;

EOT
