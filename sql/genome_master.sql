
psql -U phylomix <<EOF

/*
DROP TABLE IF EXISTS genome_master CASCADE;
CREATE TABLE genome_master (
  scientific_name TEXT PRIMARY KEY
  , common_name TEXT
  , japanese_name TEXT
  , phylum TEXT
  , subphylum TEXT
  , class TEXT
  , "order" TEXT
  , family TEXT
);

\copy genome_master FROM taxonomy.list2;

*/


EOF