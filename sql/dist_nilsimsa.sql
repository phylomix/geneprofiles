
psql -U phylomix <<'EOF'

--DROP FUNCTION IF EXISTS bitcount(int);
CREATE OR REPLACE FUNCTION bitcount(b int)
RETURNS integer
IMMUTABLE
LANGUAGE plpgsql AS $$
BEGIN
  b = (b & x'55'::int) + (b >> 1 & x'55'::int);
  b = (b & x'33'::int) + (b >> 2 & x'33'::int);
  RETURN (b & x'0f'::int) + (b >> 4 & x'0f'::int);
END
$$;

--DROP FUNCTION IF EXISTS bitcount(bit varying);
CREATE OR REPLACE FUNCTION bitcount(b bit varying)
RETURNS integer
IMMUTABLE
LANGUAGE plpgsql AS $$
--DECLARE n integer := octet_length(b)-1;
DECLARE n integer := bit_length(b)-1;
DECLARE i integer;
DECLARE c integer := 0;
BEGIN
  FOR i IN 0..n LOOP
    c := c + get_bit(b, i);
  END LOOP;
  RETURN c;
END
$$;

SELECT bitcount(base64_to_bits(a.lsh) # base64_to_bits(b.lsh)) AS bit_dist
     , count(*)
FROM nilsimsa a
INNER JOIN nilsimsa b ON a.id < b.id
GROUP BY bit_dist;
EOF
