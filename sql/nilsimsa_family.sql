psql -U phylomix <<EOF

create table nilsimsa_family as
select lsh,
array_agg(decode(md5, 'hex')) as md5
from nilsimsa
group by lsh
having count(*)>1;

EOF
