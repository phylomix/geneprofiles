
psql -U phylomix <<EOF

DROP TABLE IF EXISTS approx_distance;
CREATE TABLE approx_distance AS
SELECT decode(a1.md5,'hex') AS s1md5
	, decode(a2.md5,'hex') AS s2md5
	, length(replace((to_bits(a1.lsh)
	  # to_bits(a2.lsh) )::text, '0', ''))
	  AS distance
FROM nilsimsa a1
INNER JOIN nilsimsa a2 ON a1.id < a2.id
WHERE length(replace((to_bits(a1.lsh)
	  # to_bits(a2.lsh) )::text, '0', ''))
	< 70
;
EOF
