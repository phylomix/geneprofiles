psql -U phylomix <<'EOF'

DROP FUNCTION annotation_profile(TEXT);
CREATE OR REPLACE FUNCTION annotation_profile(a TEXT)
RETURNS TABLE(
	  common_name TEXT
	, "seq/sp" BIGINT
	, species TEXT
	, "sp/f" BIGINT
	, family TEXT
	, "f/o" BIGINT
	, "order" TEXT
	, "o/c" BIGINT
	, class TEXT
	, "c/p" BIGINT
	, phylum phyla_enum
) AS $$
SELECT DISTINCT
       common_name
     , count(gi) over (partition BY species) as seq_sp
     , species
     , count(species) over (partition BY family) as sp_f
     , family
     , count(family) over (partition BY "order") as f_o
     , "order"
     , count("order") over (partition BY class) as o_c
     , class
     , count(class) over (partition BY phylum) as c_p
     , phylum
  FROM (SELECT *, scientific_name AS species FROM seq_info1 WHERE annotation=a) x
  JOIN genome_master3 USING (scientific_name)
  ORDER BY c_p DESC, o_c DESC, f_o DESC, sp_f DESC, seq_sp DESC
;
$$ LANGUAGE sql
;

EOF
