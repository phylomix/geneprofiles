psql -U phylomix <<'EOF'

-- ipr_family, ipr_spectra_family depends on it.
DROP VIEW IF EXISTS taxonomy;
CREATE VIEW taxonomy AS
SELECT DISTINCT scientific_name
     , array_to_string(
        array[phylum::text,class,"order",family,scientific_name],';')
	||' # '||common_name
       AS taxonomy
  FROM genome_master3
;

DROP VIEW seq_std_annotation;
CREATE VIEW seq_std_annotation AS
SELECT md5, accession, scientific_name
     , array_to_string(regexp_matches(annotation, '^(?:PREDICTED: )?(.*?)(?:-like)?$'),'')
       AS annotation
     , annotation ~ '^PREDICTED: ' AS predicted
     , annotation ~ '-like$' AS _like
FROM seq_info;


--Time: 896996.791 ms
DROP TABLE IF EXISTS annotation_family;
CREATE TABLE annotation_family AS
SELECT annotation
     , string_agg(DISTINCT taxonomy, E'\n') as species_list
     , count(DISTINCT phylum) AS n_phylum
     , count(DISTINCT class) AS n_class
     , count(DISTINCT "order") AS n_order
     , count(DISTINCT family) AS n_family
     , count(DISTINCT scientific_name) AS n_species
     , count(DISTINCT accession) AS n_seq
     , array_agg(DISTINCT phylum) AS phyla
     , array_agg(DISTINCT class) AS classes
     , array_agg(DISTINCT "order") AS orders
     , array_agg(DISTINCT family) AS families
     , array_agg(DISTINCT accession) AS accessions
  FROM seq_std_annotation
  LEFT JOIN taxonomy USING (scientific_name)
  FULL JOIN genome_master3 USING (scientific_name)
 GROUP BY annotation
;
EOF
. sql/ipr_family


--CREATE INDEX ON annotation_family USING count(annotation);

/*

--CREATE INDEX ON genome_master3(scientific_name);
*/
/*
SELECT * FROM taxonomy ORDER BY scientific_name LIMIT 10;
SELECT * FROM taxonomy ORDER BY taxonomy LIMIT 10;
SELECT * FROM taxonomy ORDER BY taxonomy DESC LIMIT 10;
*/

/*
DROP FUNCTION functional_protein_family(INT, INT);
CREATE OR REPLACE FUNCTION
  functional_protein_family(ofs INT = 0, lim INT = 20)
RETURNS TABLE(annotation TEXT
	    , species_list TEXT
	    , accession TEXT[])
AS $$
WITH a AS (
SELECT annotation
     , species
     , accession
  FROM annotation_family
 LIMIT lim OFFSET ofs
)
SELECT annotation
     , string_agg(DISTINCT taxonomy, E'\n')
     , accession
  FROM a
  JOIN taxonomy ON scientific_name=ANY(species)
GROUP BY annotation, accession
;
$$ LANGUAGE sql;

SELECT *
  FROM functional_protein_family(0,3)
-- ORDER BY array_length(species,1) DESC
;
*/
EOF

