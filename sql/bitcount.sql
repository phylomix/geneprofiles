

psql -U phylomix <<'EOF'

--CREATE OR REPLACE FUNCTION bitcount(bits INT) RETURNS INT
CREATE OR REPLACE FUNCTION bitcount(bits BYTEA) RETURNS INT
LANGUAGE plpgsql
AS $$
  DECLARE i INT;
  DECLARE j INT;
  DECLARE m INT;
  DECLARE n INT;
  DECLARE L INT;
  BEGIN
    L := octet_length(bits);
    n := 0;
    FOR j IN 0..(L-1) LOOP
      m := get_byte(bits, j);
      FOR i IN 0..8 LOOP
        n := n + (m & 1); 
        m := m >> 1;
      END LOOP;
    END LOOP;
    RETURN n;
  END
$$;

CREATE OR REPLACE FUNCTION hex2bit(a BYTEA, b BYTEA)
RETURNS INT
LANGUAGE plpgsql
AS $$
  DECLARE i INT;
  DECLARE j INT;
  DECLARE m INT;
  DECLARE n INT;
  DECLARE r bit varying;
  DECLARE L INT;
  BEGIN
    L := octet_length(a);
    r := 0::bit varying;
    FOR j IN 0..(L-1) LOOP
      m := get_byte(bits, j);
      n := 0;
      FOR i IN 0..15 LOOP
        n := n + (m & 1); 
        m := m >> 1;
      END LOOP;
    END LOOP;
    RETURN n;
  END
$$;

EOF
