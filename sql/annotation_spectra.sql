psql -U phylomix <<'EOF'

CREATE MATERIALIZED VIEW annotation_spectra AS
WITH a AS (
  SELECT DISTINCT annotation, scientific_name, count(*)
  FROM seq_std_annotation
  GROUP BY annotation, scientific_name)
SELECT annotation
     , array_agg(distinct scientific_name || ':' || count) as species
  FROM a
 GROUP BY annotation;

EOF
