#!/bin/sh

#./get_sequence_header.pl | xz >sequence_file_headers.txt.xz

psql -U phylomix <<EOF

--DROP TABLE IF EXISTS seq_info;
CREATE TABLE IF NOT EXISTS seq_info (
  gi INT NOT NULL
  , accession TEXT NOT NULL
  , annotation TEXT NOT NULL
  , scientific_name TEXT NOT NULL
  , md5 BYTEA NOT NULL
  , tsv tsvector
);

CREATE TEMP TABLE tmp_seq_info (LIKE seq_info);
ALTER TABLE tmp_seq_info ALTER md5 type TEXT;

--\copy tmp_seq_info FROM PROGRAM './get_sequence_header.pl'
\copy tmp_seq_info FROM PROGRAM 'xzcat sequence_file_headers.txt.xz'

CREATE INDEX ON tmp_seq_info(gi);

INSERT INTO seq_info
SELECT DISTINCT ON (gi) gi, accession, annotation, scientific_name, decode(md5,'hex')
     setweight(to_tsvector(annotation),'A')||
     setweight(to_tsvector(scientific_name),'B')||
     setweight(to_tsvector(accession),'D');

FROM tmp_seq_info;

ALTER TABLE seq_info ADD PRIMARY KEY(gi);
CREATE UNIQUE INDEX ON seq_info(accession);
CREATE INDEX ON seq_info(md5);
CREATE INDEX ON seq_info (scientific_name);
CREATE INDEX ON seq_info USING
	GIN(to_tsvector('English', scientific_name||' '||annotation));

EOF
