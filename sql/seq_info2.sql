
psql -U phylomix <<'EOF'

/*
DROP TABLE IF EXISTS seq_info2;
CREATE TABLE seq_info2 AS
WITH a AS (
  SELECT *, regexp_matches(annotation,
  	'(PREDICTED: LOW QUALITY PROTEIN: |PREDICTED: )?(.*?)(-like, partial|-like|, partial|isoform.*)?$' ) AS x 
    FROM seq_info
)
SELECT gi,accession,scientific_name,md5
     , x[2] AS annotation
     , x[1] AS prefix
     , x[3] AS suffix
  FROM a
;

CREATE INDEX ON seq_info2 (annotation);
CREATE INDEX ON seq_info2 (scientific_name);
CREATE INDEX ON seq_info2 (md5);
CREATE INDEX ON seq_info2 USING gin(to_tsvector('English', annotation));
*/

DROP TABLE IF EXISTS annotation_spectra2;
CREATE TABLE annotation_spectra2 AS
SELECT annotation
     , array_agg(DISTINCT prefix) AS prefix
     , array_agg(DISTINCT suffix) AS suffix
     , string_agg(DISTINCT taxonomy, E'\n') AS species_list
     , count(DISTINCT phylum) AS "#phyla"
     , count(DISTINCT class) AS "#classes"
     , count(DISTINCT "order") AS "#orders"
     , count(DISTINCT family) AS "#families"
     , count(DISTINCT scientific_name) AS "#species"
     , count(DISTINCT accession) AS "#seqs"
     , array_agg(DISTINCT phylum) AS phylum
     , array_agg(DISTINCT class) AS class
     , array_agg(DISTINCT "order") AS order
     , array_agg(DISTINCT family) AS family
     , array_agg(DISTINCT accession) AS accession
--  FROM seq_std_annotation
  FROM seq_info2
  LEFT JOIN taxonomy USING (scientific_name)
  LEFT JOIN genome_master3 USING (scientific_name)
 GROUP BY annotation
;

CREATE INDEX ON annotation_spectra2 (annotation);
CREATE INDEX ON annotation_spectra2 ("#phyla");
CREATE INDEX ON annotation_spectra2 ("#classes");

/*
DROP TABLE IF EXISTS annotation_term_stat;
CREATE TABLE annotation_term_stat AS
SELECT *
FROM ts_stat('SELECT to_tsvector(''english'',annotation) from seq_info2')
ORDER BY nentry DESC;
*/


/*
create table seq_info_mt () inherits (seq_info);
create table seq_info_chl () inherits (seq_info);
with a as (
  delete from seq_info
  where annotation ~ '(mitochondrion)'
  returning *
)
insert into seq_info_mt
SELECT * FROM a;
with a as (
  delete from seq_info
  where annotation ~ '(chloroplast)'
  returning *
)
insert into seq_info_chl
SELECT * FROM a;

with a as (
  delete from seq_info
  where annotation ~ 'mitochondrial|mitochondrion'
  returning *
)
INSERT INTO seq_info_mt
SELECT * FROM a
;

with a as (
  delete from seq_info
  where annotation ~ 'mitochondria protein'
  returning *
)
INSERT INTO seq_info_mt
SELECT * FROM a
;

CREATE UNIQUE INDEX ON seq_info_mt(accession);
CREATE INDEX ON seq_info_mt(md5);
CREATE INDEX ON seq_info_mt (scientific_name);
CREATE INDEX ON seq_info_mt USING
        GIN(to_tsvector('English', scientific_name||' '||annotation));

CREATE UNIQUE INDEX ON seq_info_chl(accession);
CREATE INDEX ON seq_info_chl(md5);
CREATE INDEX ON seq_info_chl (scientific_name);
CREATE INDEX ON seq_info_chl USING
        GIN(to_tsvector('English', scientific_name||' '||annotation));

*/
EOF
