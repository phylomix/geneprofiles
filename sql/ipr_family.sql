
psql -U phylomix <<EOF

DROP MATERIALIZED VIEW ipr_family;
CREATE MATERIALIZED VIEW ipr_family AS
SELECT ipr_id, ip.annotation, go_term
     , array_agg(DISTINCT taxonomy) AS species_list
     , count(DISTINCT phylum) AS phylum
     , count(DISTINCT class) AS class
     , count(DISTINCT "order") AS order
     , count(DISTINCT family) AS family
     , count(DISTINCT scientific_name) AS species
     , count(DISTINCT s.md5) AS uniq_seq
     , count(s.md5) AS seq
  FROM interpro ip
  LEFT JOIN interpro_seq i USING (ipr_id)
  LEFT JOIN seq_info s ON s.md5=ANY(i.md5)
  LEFT JOIN taxonomy USING (scientific_name)
  LEFT JOIN genome_master3 USING (scientific_name)
 GROUP BY ipr_id, ip.annotation, go_term;

EOF
