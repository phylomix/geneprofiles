
psql -U phylomix <<EOF

CREATE TEMP table tmp (
  md5 TEXT
  lsh TEXT
)

\copy nilsim FROM PROGRAM './nilsimsa.pl|sort -u';

CREATE TABLE nilsim AS
SELECT decode(md5, 'hex') AS md5
     , lsh
);

EOF
