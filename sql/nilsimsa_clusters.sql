
psql -U phylomix <<'EOF'

WITH a AS (
  SELECT * FROM nilsimsa_family LIMIT 5
)
SELECT a.lsh
     , string_agg(DISTINCT accession,' ') AS accession
     , string_agg(DISTINCT scientific_name,' ') AS species
     , string_agg(DISTINCT annotation,' ') AS annotation
  FROM a
  JOIN seq_info s ON s.md5=ANY(a.md5)
  GROUP BY a.lsh 
;

EOF
