
psql -U phylomix <<'EOF'

DROP FUNCTION bytea_to_bits(bytea);
CREATE OR REPLACE FUNCTION bytea_to_bits(s bytea)
RETURNS bit varying
IMMUTABLE
LANGUAGE plpgsql AS $$
DECLARE n int := octet_length(s)-1;
DECLARE i int;
DECLARE r bit varying := '';
BEGIN
  for i IN 0..n LOOP
    r := r || get_byte(s, i)::bit(8);
  END LOOP;
  RETURN r;
END
$$;

CREATE OR REPLACE FUNCTION bytea_to_bits(s text)
RETURNS bit varying
IMMUTABLE
LANGUAGE sql AS $$
SELECT bytea_to_bits(decode(s,'hex'));
$$;

CREATE OR REPLACE FUNCTION base64_to_bits(s text)
RETURNS bit varying
IMMUTABLE
LANGUAGE sql AS $$
SELECT bytea_to_bits(decode(s,'base64'));
$$;

--SELECT base64_to_bits(lsh) from nilsimsa limit 10;

EOF
