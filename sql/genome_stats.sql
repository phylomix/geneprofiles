
DROP TABLE IF EXISTS public.genome_stats CASCADE;
CREATE TABLE public.genome_stats (
  scientific_name TEXT PRIMARY KEY
  , N_genes INT
  , len_mean   FLOAT
  , len_stdev FLOAT
  , len_log_mean FLOAT
  , len_log_stdev FLOAT
);

\copy genome_stats FROM PROGRAM 'sed "1d;s/_/ /g" genome_stats.txt'


DROP TABLE IF EXISTS public.blastclust_stats CASCADE;
CREATE TABLE public.blastclust_stats (
  scientific_name TEXT PRIMARY KEY
  , N_clusters INT
  , clst_max_size  INT
  , clst_mean_size   FLOAT
  , clst_stdev FLOAT
  , clst_log_mean FLOAT
  , clst_log_stdev FLOAT
);

\copy blastclust_stats FROM PROGRAM 'sed "1d;s/_/ /g" blastclust_stats.txt'



--DROP VIEW public.genome_master_stats;
DROP VIEW genome_master_stats;
CREATE VIEW public.genome_master_stats AS
SELECT DISTINCT *
--FROM genome_master
FROM genome_data_master
NATURAL FULL JOIN genome_master gm
NATURAL FULL JOIN genome_stats
NATURAL FULL JOIN blastclust_stats
NATURAL LEFT JOIN scientific_name_to_tax_id
ORDER BY phylum, subphylum, class, "order", family, scientific_name, common_name
;


DROP VIEW genome_table;
CREATE VIEW genome_table AS
SELECT phylum, subphylum, "class", "order", family
     , scientific_name
     , taxon_id
     , common_name
     , japanese_name
     , n_genes, len_mean, len_stdev, len_log_mean, len_log_stdev
     , n_clusters, clst_max_size, clst_mean_size, clst_stdev, clst_log_mean, clst_log_stdev
FROM genome_master
NATURAL FULL JOIN genome_stats
NATURAL FULL JOIN blastclust_stats
--NATURAL LEFT JOIN scientific_name_to_taxon_id
ORDER BY phylum, subphylum, class, "order", family, scientific_name, common_name
;
