
psql -U phylomix <<'EOF'

DO language plpgsql $$ BEGIN RAISE INFO 'Before indexing'; END $$;
EXPLAIN SELECT DISTINCT annotation
     , string_agg(DISTINCT accession, ' ') AS accession
     , string_agg(DISTINCT scientific_name, '; ') AS species
     , array_agg(DISTINCT md5) as md5hash
 FROM seq_info
 GROUP BY annotation
;

DO language plpgsql $$ BEGIN RAISE INFO 'Creating index'; END $$;
CREATE INDEX ON seq_info(annotation);
DO language plpgsql $$ BEGIN RAISE INFO 'After indexing'; END $$;
EXPLAIN SELECT DISTINCT annotation
     , string_agg(DISTINCT accession, ' ') AS accession
     , string_agg(DISTINCT scientific_name, '; ') AS species
     , array_agg(DISTINCT md5) as md5hash
 FROM seq_info
 GROUP BY annotation
;


CREATE TABLE annotation_agg AS
SELECT DISTINCT annotation
     , string_agg(DISTINCT accession, ' ') AS accession
     , string_agg(DISTINCT scientific_name, '; ') AS species
     , array_agg(DISTINCT md5) as md5hash
 FROM seq_info
 GROUP BY annotation
;
EOF
