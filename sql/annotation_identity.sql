psql -U phylomix <<'EOF'

/*
DROP VIEW taxonomy CASCADE;
CREATE VIEW taxonomy AS
SELECT DISTINCT scientific_name
     , array_to_string(
        array[phylum::text,class,"order",family,scientific_name],';')
	||' # '||common_name
       AS taxonomy
  FROM genome_master3
;
*/

DROP TABLE IF EXISTS annotation_identity;
CREATE TABLE annotation_identity AS
SELECT annotation
     , string_agg(DISTINCT taxonomy, E'\n') as species_list
     , count(DISTINCT phylum) AS phylum
     , count(DISTINCT class) AS class
     , count(DISTINCT "order") AS order
     , count(DISTINCT family) AS family
     , count(DISTINCT scientific_name) AS species
     , count(DISTINCT md5) AS sequence
     , string_agg(DISTINCT accession, ' ') AS accession
  FROM seq_info
  JOIN taxonomy USING (scientific_name)
  JOIN genome_master3 USING (scientific_name)
 GROUP BY annotation
;

--CREATE INDEX ON annotation_family USING count(annotation);

/*

--CREATE INDEX ON genome_master3(scientific_name);
*/
/*
SELECT * FROM taxonomy ORDER BY scientific_name LIMIT 10;
SELECT * FROM taxonomy ORDER BY taxonomy LIMIT 10;
SELECT * FROM taxonomy ORDER BY taxonomy DESC LIMIT 10;
*/

/*
DROP FUNCTION functional_protein_family(INT, INT);
CREATE OR REPLACE FUNCTION
  functional_protein_family(ofs INT = 0, lim INT = 20)
RETURNS TABLE(annotation TEXT
	    , species_list TEXT
	    , accession TEXT[])
AS $$
WITH a AS (
SELECT annotation
     , species
     , accession
  FROM annotation_family
 LIMIT lim OFFSET ofs
)
SELECT annotation
     , string_agg(DISTINCT taxonomy, E'\n')
     , accession
  FROM a
  JOIN taxonomy ON scientific_name=ANY(species)
GROUP BY annotation, accession
;
$$ LANGUAGE sql;

SELECT *
  FROM functional_protein_family(0,3)
-- ORDER BY array_length(species,1) DESC
;
*/
EOF

