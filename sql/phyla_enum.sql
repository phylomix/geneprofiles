
psql -U phylomix <<'EOF'

CREATE TYPE phyla_enum AS ENUM (
  'Chordata', 'Hemichordata', 'Echinodermata', 'Priapulida', 'Nematoda', 'Arthropoda', 'Platyhelminthes', 'Brachiopoda', 'Mollusca', 'Annelida', 'Cnidaria', 'Placozoa', 'Porifera', 'Opisthokonta', 'Ascomycota', 'Streptophyta'
);
EOF
