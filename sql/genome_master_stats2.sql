
psql -U phylomix <<'EOF'

DROP VIEW IF EXISTS genome_master2 CASCADE;
CREATE OR REPLACE VIEW genome_master2 AS
 SELECT DISTINCT
    taxon_id,
    scientific_name,
    common_name,
    japanese_name,
    phyid,
    subphylum,
    infraphylum,
    class,
    "order",
    family,
    gdm.synonym,
    gdm.ebi,
    gdm.ucsc,
    gdm.ncbi,
    gdm.comment
   FROM genome_data_master gdm
   FULL JOIN genome_master gm
    USING (taxon_id, scientific_name, phyid, subphylum,
          class, "order", family, common_name,japanese_name)
  GROUP BY
    taxon_id,
    scientific_name,
    common_name,
    japanese_name,
    phyid,
    subphylum,
    class,
    "order",
    family,
    gdm.synonym,
    gdm.infraphylum,
    gdm.ebi,
    gdm.ucsc,
    gdm.ncbi,
    gdm.comment
  ORDER BY phyid, class, "order", family, scientific_name
;

DROP TABLE IF EXISTS genome_master3 CASCADE;
CREATE TABLE genome_master3 AS
 SELECT DISTINCT
    taxon_id,
    scientific_name,
    string_agg(common_name, ' / ') AS common_name,
    string_agg(japanese_name, ' / ') AS japanese_name,
    phyid AS phylum,
    string_agg(subphylum, ' / ') AS subphylum,
    infraphylum,
    class,
    "order",
    family,
    synonym,
    string_agg(ebi, '; ') AS ebi,
    string_agg(ucsc, '; ') AS ucsc,
    string_agg(ncbi, '; ') AS ncbi,
    string_agg(comment, '; ') AS comment
   FROM genome_master2
  GROUP BY
    taxon_id,
    scientific_name,
    --common_name,
    --japanese_name,
    phylum,
    --subphylum,
    infraphylum,
    class,
    "order",
    family,
    synonym
  ORDER BY phylum, class, "order", family, scientific_name
;

SELECT * FROM genome_master3 WHERE taxon_id=8090;

CREATE OR REPLACE VIEW public.genome_master_stats2 AS
SELECT DISTINCT *
FROM genome_master3
NATURAL FULL JOIN genome_stats
NATURAL FULL JOIN blastclust_stats
NATURAL LEFT JOIN scientific_name_to_tax_id
ORDER BY phylum, subphylum, class, "order", family, scientific_name, common_name
;


--SELECT * FROM genome_master_stats2 WHERE taxon_id=8090;

EOF
