#!/bin/bash

psql -U phylomix <<EOF
CREATE TABLE acc_seq_md5 (
  accession TEXT PRIMARY KEY
  , seq_md5 TEXT
  , length INT
);

\copy acc_seq_md5 FROM PROGRAM 'e.sh'

SELECT seq_md5
     , string_agg(accession, ' ')
     , string_agg(length::text, ' ')
FROM acc_seq_md5
GROUP BY seq_md5
HAVING COUNT(*) >2
;
EOF
