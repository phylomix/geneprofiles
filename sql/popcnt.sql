

psql -U phylomix <<'EOF'

CREATE OR REPLACE FUNCTION popcnt(b bytea)
RETURNS INT AS $FUNCTION$
DECLARE i INT;
DECLARE n INT :=0;
DECLARE len INT := octet_length(b) -1;
BEGIN
  FOR i in 0..len LOOP
    n := n + length(replace(get_byte(b,i)::bit(8)::text,'0',''));
  END LOOP;
  RETURN n;
END $FUNCTION$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION popcnt2(b bytea)
RETURNS INT AS $FUNCTION$
-- pit fall - offset 1 instead of 0
DECLARE BITS INT[] := '{
    0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8
}';
DECLARE i INT;
DECLARE n INT := 0;
DECLARE len INT := octet_length(b) - 1;
BEGIN
  FOR i IN 0..len LOOP
    n := n + BITS[ 1 + get_byte(b,i) ];
  END LOOP;
  RETURN n;
END $FUNCTION$ LANGUAGE plpgsql;


SELECT distinct popcnt (decode('ffffff','hex')) FROM generate_series(1,1000);
SELECT distinct popcnt2(decode('ffffff','hex')) FROM generate_series(1,1000);
SELECT distinct to_bits(decode('ffffff','hex')) FROM generate_series(1,1000);

EOF
