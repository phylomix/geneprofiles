#!/usr/bin/perl
#

use BerkeleyDB;

my $db_file = shift;
my $db = tie %hash, 'BerkeleyDB::Btree', -Filename=>$db_file, -Flags=>DB_CREATE ;


while (<>) {
    my ($key, $val) = split;
    $key =~ s/gi.*\|(.+)\|/$1/;
    $val or next;
    if (defined $hash{$key}) {
    	$hash{$key} =~ /\Q$val\E/ and next;
	$hash{$key} .= ','. $val;
    } else { 
	$hash{$key} = $val;
    }
    print "$key -> $hash{$key}\n";
}


$db->db_close;
