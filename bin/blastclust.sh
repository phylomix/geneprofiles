#!/bin/bash

NCPU=30
LENGTH=0.7
SCORE=50
WORDSIZE=3
BCLUST_CMD="blastclust -a $NCPU -L $LENGTH -S $SCORE -W $WORDSIZE -e F"
TIME=/usr/bin/time

for species in `cat SPECIES`; do
	(
	cd $species
	echo -n "blastclust $species "
	stem=$species
	blastclust=$stem.blastclust
	neighbours=$stem.neighbours
	src=protein/protein.fa.gz
	if [ -s $blastclust -a $src -ot $blastclust ]; then
		echo skipped
	else
		echo exec
		zcat $src | $TIME $BCLUST_CMD -o $blastclust -s $neighbours | xargs echo
	fi
	)
done
