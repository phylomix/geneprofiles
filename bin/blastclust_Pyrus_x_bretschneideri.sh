#!/bin/sh
stem=Pyrus_x_bretschneideri

zcat $stem/protein/protein.fa.gz |
blastclust -a 10 -L 0.7 -S 50 -W 3 -e F -o $stem/$stem.blastclust -s $stem/$stem.neighbours\
&>$stem/blastclust.log & 
