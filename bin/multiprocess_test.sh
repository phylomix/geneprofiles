#!/bin/sh

Nproc=10

for i in `seq 1 $Nseq`; do
	sleep $i&
	proc="$proc $!"
done

debug() { echo "DEBUG: $*" >&2; }

errors=0
while :; do
	for pid in $proc; do
		if kill -0 "$pid" 2>/dev/null; then
			debug "$pid is still running.";
			set -- "$pooc" "$pid"
		elif wait "$pid"; then
			debug "$pid finished successfully."

		else
			debug "$pid exited on error."
			((++errors))
		fi
	done
	(("$proc" > 0)) || break
	sleep 5
done


