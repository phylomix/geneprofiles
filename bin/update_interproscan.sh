#!/bin/bash

function fetch_extract() {
	echo downloading $1
	wget -q -nd $1{,.md5}
	md5sum -c $1.md5 || (echo download error; exit 1)
	rm -f $1.md5
	tar pxzof $1
}


# main program
url=https://www.ebi.ac.uk/interpro/interproscan.html
archive=`wget -q -O - $url | grep download_cont | sed 's/.*\(ftp:.*tar.gz.\).*/\1/'`
version=`basename $archive -64-bit.tar.gz`
#fetch_extract $archive

# panther
(
#cd $version/data
cd /usr/local/bin/interproscan_current/data
url=ftp://ftp.ebi.ac.uk/pub/software/unix/iprscan/5/data/
data=`wget -q -O - $url | grep 'File' | sed '2!d;s/.*\(ftp:.*.tar.gz\)".*/\1/'`
fetch_extract $data
)

# lookup service - it requires ~300G
url=https://github.com/ebi-pf-team/interproscan/wiki/LocalLookupService
lookup=`wget -q -O - $url | grep 'Direct link:' | sed 's/.*\(ftp:.*tar.gz.\).*/\1/'`
#fetch_extract lookup

