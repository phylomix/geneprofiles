#!/bin/bash

DEBUG=""
#DEBUG="--dry-run"
export IPROPTS="--goterms --iprlookup --pathways --formats TSV, XML"
export IPRSCAN="/usr/local/interproscan/interproscan.sh $IPROPTS"
export LOG=interproscan.`date +%F`.log
export FA_GZ=protein/protein.fa.gz

iprscan(){
	species=$1
	cd $species
	echo -n "$species interproscan "

	# remove previous log if execution failure recorded
	# for memory outage
	grep -l -E 'insufficient memory|errors' *.log | xargs rm -f

	# check previous log, if it exists, to see freshness
	log0=`ls interproscan*log 2>/dev/null |head -1`
	if [ -s $species.fa.tsv -a ! -e $species.fa -a -s "$log0" -a "$log0" -nt $FA_GZ ]
	then
		echo "up to date or running."
	else
		gzip -cd $FA_GZ >$species.fa
		/usr/bin/time $IPRSCAN -i $species.fa 2>&1 | xargs echo >$LOG
		rm -f $species.fa
	fi
	cd ..
}
export -f iprscan

# execution in semaphore
for species in `cat SPECIES`
do
	echo queuing $species
	sem --id interpro.sh -j10 $DEBUG iprscan $species
done
