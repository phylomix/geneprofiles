#!/bin/sh

cat *.tsv | awk 'BEGIN{FS="\t"};{print $1, $14}' | uniq | mk_acc2go_dic.pl $@
