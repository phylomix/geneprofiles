#!/bin/sh

IPRSCAN="/usr/local/interproscan/interproscan.sh -goterms -iprlookup -pathways -formats TSV, XML"
species=$1
for species in $* ; do
	cd $species
	if [ -s $species.xml ]; then
		echo $species: $species.xml existed
	else
		gzip -cd protein/protein.fa.gz >$species.fa
		nohup $IPRSCAN -i $species.fa >interproscan.log &
	fi
	cd ..
done
