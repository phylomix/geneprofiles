#!/bin/bash

IPRSCAN="/usr/local/interproscan/interproscan.sh --goterms --iprlookup --pathways --formats TSV, XML"

for species in `cat SPECIES2`; do
	stem=$species/$species
	echo -n "$species interproscan "
	if [ -s $stem.xml -a $stem.fa.gz -ot $stem.xml ]; then
		echo "up to date."
	else 
		gzip -cd $species/protein/protein.fa.gz >$stem.fa
		/usr/bin/time $IPRSCAN -i $stem.fa >>log/interproscan.`date +%F`.log
		rm -f $stem.fa
	fi
done
