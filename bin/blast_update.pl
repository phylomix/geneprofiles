#!/bin/sh

repo=ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST

wget -Nc -nv $repo/ncbi-blast-*+-x64-linux.tar.gz{,.md5}

archive=`ls ncbi-blast-*+-x64-linux.tar.gz`
stem=`basename $archive -x64-linux.tar.gz`
md5sum -c $archive.md5 || ( echo "Download error!"; exit 1 )

if [ `which blastp` -ot $archive ]; then
	tar xzof $archive */bin
	sudo install $stem/bin/* /usr/local/bin
fi

rm -rf $stem $archive{,.md5}
