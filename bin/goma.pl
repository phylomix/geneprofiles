#!/usr/bin/perl

use List::Util qw/sum/;
use Getopt::Long;
use JSON;
use CGI::Pretty qw/:all *table/;
use CGI::Carp;

use constant QuickGO => 'http://www.ebi.ac.uk/QuickGO/GTerm?id=GO:';
use constant GoSuffix => '#term=ancchart';

my $TYPE = "text";
GetOptions("type=s" => \$TYPE);
$TYPE = 'html' if defined $ENV{'HTTP_ACCEPT'};

my @SPECIES;
my %C;  # GO term dictionary

for (`cat go.dic`) {
    chomp;
    @_ = split /\t/;
    $C{$_[0]} = $_[1];
}


my @files = @ARGV;
@files = <*.gosp>, <*.gosp.txt> if 0 == 0 + @files;

# count up
for my $go ( @files ) {
    (my $species = $go) =~ s/.protein(.fa)?.gosp(\d+|.txt)?$//;
    $species =~ s/^KH.//;
    $species =~ s/.Met$//;
    print STDERR $species, "\n" if $TYPE eq 'text';
    push @SPECIES, $species;
    for (`cat $go`) {
	chomp;
	@_ = split;
	$GO{$_[1]}{$species}=$_[0];  # frequency, go_id
    }
}

sub output_json($\%) {
    my ($key, $GO) = @_;

    print $key;
    my @keys = grep {$C{$_} =~ /$key/o} keys %$GO;
    print @keys; exit;
    my @top = sort { hash_sum($GO,$b)<=>hash_sum($GO,$a) } @keys;
    print encode_json [@top];
}

sub ns($) { substr $C{ $_[0] } || $C{ "GO:".$_[0] }, 0, 2 }
sub hash_sum ($$) { sum values %{$_[0]->{$_[1]}} }

sub order($) {
    my $GO = shift;
    sort { ns($a) cmp ns($b)
	   || hash_sum($GO, $b) <=> hash_sum($GO, $a)
    } keys %$GO 
}



sub output_tabtext(\%\@) {
    my ($GO, $SPECIES) = @_;

    #print STDERR "tabtexts";

    # header
    #print join "\t", 'GO_ID', 'name', 'TOTAL', @$SPECIES;
    #print join "\t", 'GO_ID', 'name', @$SPECIES;
    my @labels = @$SPECIES;
    map s/\.gosp.?$//, @labels;
    map s/\.fa$//, @labels;
    print join "\t", 'GO_ID', 'GO Annotation', @labels;
    print "\n";
    
    # main table
    for my $go_id (order $GO) {
        my $goid = $go_id;
	$goid = "GO:$goid" if $goid !~ /^GO:/;
	print join "\t",
	           $goid, $C{$goid},
		   #hash_sum( %$go, $go_id ), # total
	           map($GO->{$go_id}{$_}, @$SPECIES),
		   "\n";
    }
}


my @jslib = qw(
  https://code.jquery.com/jquery-2.1.3.min.js
  js/jquery.tablesorter.min.js
  js/addons/pager/jquery.tablesorter.pager.js
  js/jquery.balloon.min.js
);


my $jscript = <<'EOT';
$(document).ready(function() {
  $("#table")
    .tablesorter({widthFixed: true, widgets: ['zebra']})
    .tablesorterPager({container: $("#pager")});
  $("#go_out").baloon({
    contents: this.href,
    url: this.href
  });
});
EOT

use constant pager => <<'EOT';
<div id="pager" class="pager">
    <form>
    <img src="js/addons/pager/icons/first.png" class="first"/>
    <img src="js/addons/pager/icons/prev.png" class="prev"/>
    <input type="text" class="pagedisplay"/>
    <img src="js/addons/pager/icons/next.png" class="next"/>
    <img src="js/addons/pager/icons/last.png" class="last"/>
    <select class="pagesize">
    <option selected="selected"  value="50">50</option>
    <option value="20">100</option>
    <option value="200">200</option>
    <option  value="500">500</option>
    </select>
    </form>
</div>
EOT

sub output_html(\%\@) {
    my ($GO, $SPECIES) = @_;

    print header, start_html(-script=>[map( {{-src=>$_}} @jslib ),
				       {-code=>$jscript}],
			     -style=>{-src=>'js/themes/blue/style.css'} );

    print pager;
    print start_table({-id=>'table', -class=>'tablesorter'});

    # table head
    my @head = @$SPECIES;
    map s/_/ /, @head;
    print thead( Tr([ th([ 'ID', 'name', 'TOTAL', @head ]) ]) ),"\n";

    # table body
    print "<tbody>\n";
    for my $go_id (order $GO) {
	print Tr([ td([a({-id=>'go_out', -href=>QuickGO.$go_id.GoSuffix},"GO:$go_id")]).
		   td({-class=>'name'},[$C{"GO:$go_id"}]).
		   td([hash_sum( $GO, $go_id ),
		       map $GO->{$go_id}{$_}, @$SPECIES
		      ]) ]);
	print "\n";
    }
    print "</tbody>", end_table, pager, end_html;
}

sub output_chart(\%\@) {
}

sub output_csv(\%\@) {
    my ($GO, $SPECIES) = @_;

    # header
    print '"', join '","', 'GO ID', 'TOTAL', @$SPECIES, 'GO Annotation';
    print "\"\n";
    
    # main table
    for my $go_id ( order $GO ) {
	print '"', join '","',
  	           "GO:$go_id",
#	           hash_sum( %$go, $go_id ),
	           map $GO->{$go_id}{$_}, @$SPECIES,
	           $C{"GO:$go_id"};
	print "\"\n";
    }
}


if ($TYPE eq 'json') {
    output_json(qr/^BP:/, %GO);
} elsif ($TYPE eq 'html') {
    output_html(%GO,@SPECIES)
} elsif ($TYPE eq 'chart') {
    output_chart(%GO,@SPECIES)
} elsif ($TYPE eq 'csv') {
    output_csv(%GO,@SPECIES)
} else {
    output_tabtext(%GO,@SPECIES);
}
