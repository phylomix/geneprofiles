#!/bin/bash

cat SPECIES |
sed 's,^,ftp://ftp.ncbi.nlm.nih.gov/genomes/,;s,$,/protein/,' |
wget -crN -nH -nv --cut-dirs=1 -A "protein.fa.gz" -i -

