#!/bin/bash

countup(){
	key=$1
	shift;
	find . -name '*.fa' -exec grep $key '{}' \; |
	cut -d' ' -f 2- |
	sed "s/ $key /\t/;s/ gene product *//" |
	awk 'BEGIN{FS="\t";PROCINFO["sorted_in"]="@ind_str_asc"};
	     {++N[$1][$2]; ++S[$2];}
	     END{
	       # column names
	       printf "-";
	       for (j in S) printf "\t%s",j;
	       print "Sum";
	       # row names and values
	       for (i in N) {
	         c=0;
		 printf i;
		 for (j in S){ printf "\t%d",N[i][j]; c+=N[i][j];}
		 print "\t" c;
	       }
	       # column values
	       c=0;
	       printf "Total\t";
	       for (j in S) {printf "%s\t", S[j]; c+=S[j];}
	       print c;
	     }'
}

countup $*
