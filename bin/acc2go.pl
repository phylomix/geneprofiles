#!/usr/bin/perl
#

use BerkeleyDB;

my $db_file = shift;
my $db = tie %hash, 'BerkeleyDB::Btree', -Filename=>$db_file;
#, -Flags=>DB_CREATE ;


while (<>) {
  chomp;
  my @acc = split;
  print join " ", map "$_($hash{$_})", @acc;
  print "\n";
}

$db->db_close;
