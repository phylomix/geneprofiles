#!/bin/sh
stem=uniref50
fasta=${stem}.fasta
url=http://ftp.ebi.ac.uk/pub//databases/uniprot/current_release/uniref/uniref50/${fasta}.gz
makeblastdb="makeblastdb -dbtype prot -parse_seqids -hash_index -max_file_sz 30GB \
	     -title $stem -out $stem -logfile $stem.makeblastdb_log"

mkdir -P $stem
cd $stem
wget -Nc -nv $url || exit 1

if [ $stem.pal -ot ${fasta}.gz ] then
	echo regenerating database
	gzip -cd ${fasta}.gz | ${makeblastdb}
fi
cd ..

exit 0

  makeblastdb [-h] [-help] [-in input_file] [-input_type type]
      -dbtype molecule_type [-title database_title] [-parse_seqids]
      [-hash_index] [-mask_data mask_data_files] [-mask_id mask_algo_ids]
      [-mask_desc mask_algo_descriptions] [-gi_mask]
      [-gi_mask_name gi_based_mask_names] [-out database_name]
      [-max_file_sz number_of_bytes] [-logfile File_Name] [-taxid TaxID]
      [-taxid_map TaxIDMapFile] [-version]

