#!/usr/bin/perl -w

use constant MIN_LENGTH => 10;
use Digest::MD5 qw(md5);
use Digest::SHA qw(sha1);

our %ID = ();
our %SEQ_HASH = ();


sub output($$;$) {
    my ($id, $seq, $note) = @_;

    if ((()= $seq =~ /\S/g) < MIN_LENGTH) {
	warn ">$id $note\n$seq";
	return
    }

    # replace vertical line to space for blastclust
    $id =~ s/\|/ /;
    
    # remove trailing asterisk
    $seq =~ s/\*$//s;  # trailing aster

    # replace irregular characters
    $seq =~ s/[^acdefghiklmnpqrstvwy\s]/x/ig;

    print ">$id $note\n$seq";
}

sub id_note_seq($) {
   my ($header, $seq) = split /\n/, $_[0], 2;
   if ((()=$header =~ /\|/g) >2) {
     #>gi|762069470|ref|XP_011426503.1|
     my ($id, $note) = split ' ', $header, 2;
     $id =~ s/\|$//;
     return ((split /\|/, $id)[-1], $note, $seq);
   } else {
     $header =~ s/^>.{2,3}\|/>/;  # remove gb style ugly prefix.
     $header =~ /^>([^ |\n]+)([^\n]*)\n(.*)\n?/s;
     return ($1, $2, $3); # id, note, seq
   }
}


sub new_id($) {
    my ($id)=@_;
    $new_id = sprintf "%s_%s", $id, $ID{$id};
    warn "Dup: $new_id\t$SEQ_HASH($id)\n";
    return $new_id;
}


sub is_unique_id_seq($$){
    my ($id, $seq)=@_;
    if (defined $ID{$id}) {
	return 0 if $SEQ_HASH{$id} eq md5($seq).sha1($seq);
	for my $i (1..$ID{$id}) {
	    return 0 if $SEQ_HASH{$id."_$i"} eq md5($seq).sha1($seq);
	}
	my $new_id = new_id $id;
	$SEQ_HASH{$new_id} = md5($seq).sha1($seq);	
	return $new_id;
    } else {
	$ID{$id} = 1;
	$SEQ_HASH{$id} = md5($seq).sha1($seq);
	return $id;
    }
}

sub main() {
    open $fh, "sed -e '1b;/^>/i\n' @ARGV |" or die;

    $/ = "";
    while (<$fh>) {        
	my ($id, $note, $seq) = id_note_seq $_;
	$seq eq '' and die "$id,$note,$seq\n";

        $seq =~ s/\n$//;  # remove trailing blank

	$newid = is_unique_id_seq($id, $seq);
 	output($newid ? $newid : $id,$seq,$note);
    }
    close $fh;
}


main();
