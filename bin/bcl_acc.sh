#!/bin/bash

isuffix=blastclust
osuffix=bcl_acc

for i in *.${isuffix}; do
	stem=`basename $i ${isuffix}`
	acc2go.pl acc2go.dic $i >${osuffix}
done


