#!/bin/bash

subproc(){
  echo $@
}
export -f subproc

seq 1 5 | parallel subproc
echo `seq 1 5` | parallel subproc

