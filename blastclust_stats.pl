#!/usr/bin/perl -w
#

use feature 'say';

my @FILES = <*/*.blastclust>;

sub expect(@) {
  my @x = @_;   # $x[0] is the number of samples
  my $e1 =  $x[1] / $x[0];   # mean
  my $e2 = ($x[2] / $x[0]) - $e1 **2;  # var
  #my $e3 = ($x[3] / $x[0]) - 3* $e1 * $e2 + 2* ($e1 **3); 
  #my $e4 = ($x[4] / $x[0]) - 4* $e1 * $e3   # kurtosis
  #           + 6* ($e1 **2) * $e2 - 3* ($e1 **4);
  #return ($e1, $e2, $e3, $e4);
  return ($e1, $e2);
}

sub lognorm(@)  {
  my ($m,$v) = @_;
  my $e1 = exp($m+$v/2);
  my $e2 = (exp($v) - 1) * exp(2*$m+$v);
  return ($e1, $e2);
}

# gene count distribution stats
sub elem_count_dist($) {
  my $file = shift;
  my @x;
  my $max=0;
  open my $fh, $file or die;
  while (<$fh>)  {
    my $cnt = (split);
    $max = $cnt if $max < $cnt;
    for my $i (1..4) {
      $x[$i]  += $cnt ** $i;
      $lx[$i] += log($cnt) ** $i;
    }
    ++ $x[0];   # counts
    ++ $lx[0];  # counts
  }
  close $fh;
  my @e = expect(@x);
  my @le = lognorm(expect(@lx));

#  return ($e[0], $e[1], sqrt($e2), $e3 **(1/3), $e4 **(1/4));
  return ($x[0], $max, $e[0], sqrt($e[1]), $le[0], sqrt($le[1]));
}

sub main {
  say join "\t", qw/scientific_name n_cluster max mean stdev log_mean log_stdev/;

  for my $f (@FILES) {
    (my $species = $f) =~ s|/.*||;
    my @dist = elem_count_dist($f);
    say join "\t", $species, splice(@dist,0,2), map {sprintf "%.2f", $_} @dist;
  }
}

main();
