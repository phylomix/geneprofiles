PATH := bin:${PATH}
VPATH := bin:${VPATH}
TIME := $(shell date)

.PHONY: wget blastclust interpro blast_update

all: blast_update index.html SPECIES wget interpro acc2go.dic

# update blast command
blast_update: ; blast_update.sh

# fetching species list
index.html: ; wget -N ftp://ftp.ncbi.nlm.nih.gov/genomes/ &>log/index.${TIME}.log

# species list
SPECIES: index.html
	sed 's/.*>\([^ /<]*\)[/<].*/\1/;t;d' $< \
	| grep '_' | grep '[a-z]' \
	| grep -Ev '^README|old_genome|DRAFT' >$@

# fetching fasta file for each speices
#wget: ; wget.sh &>log/wget.${TIME}.log
ACCEPT  := "protein.fa.gz,ref_*_top_level.gff3.gz"
EXCLUDE := all,MapView,Viruses,"*/ARCHIVE/*","[A-Z][A-Z]*"
REPO    := ftp://ftp.ncbi.nlm.nih.gov/genomes/[V-Z]*
WGET:= wget -m -nH -nv -l3 --cut-dirs=1 -A $(ACCEPT) -X $(EXCLUDE) $(REPO)
fetch:; $(WGET)


# clustering by blastclust for each species
blastclust: ; blastclust.sh &>log/blastclust.${TIME}.log

# interpro for each species
interpro: ; interpro.sh &>log/interpro.${TIME}.log

# making acc2go dictionary for accession to GO
acc2go.dic: $(wildcard *.tsv); mkacc2go.sh &>log/mkacc2go.${TIME}.log

count.mitochondrion.txt: matrix.pl; $< '(mitochondrion)' >$@
count.mitochondrial.txt: matrix.pl; $< 'mitochondrial' >$@

ncbi/gene_info.gz:
	wget -nv -Pncbi -nd ftp://ftp.ncbi.nlm.nih.gov/gene/DATA/gene_info.gz

#count.mitochondrion.txt: ; matrix.sh '(mitochondrion)' >$@
#count.mitochondrial.txt: ; matrix.sh 'mitochondrial' >$@

