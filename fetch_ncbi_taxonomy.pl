#!/usr/bin/perl -w

use feature 'say';
use LWP::UserAgent;
use HTTP::Request::Common;

sub fetch_ncbi($) {
    my $name = shift;
    my $url = 'http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi';
    my %data = ( 'name' => $name,
		 #/ 1: complete name, 2: wildcard, 3: token set,
		 #/ 4: phonetic name, 5: taxonomyid
		 'srchmode' => '1',
		 'mode'=>'Undef',
	);
    my $req = POST($url, \%data );
    
    my $ua = LWP::UserAgent->new;
    my $res = $ua->request($req)->as_string;
    return $res;
}

sub html_essence($) {
    my $data = shift;
    my $pat1 = 'TITLE="';
    my $pat2 = '".*?>(.+?)<\/A>';

    my @categories = ('phylum','subphylum','class','order','family');
    my @category;
    foreach my $c ( @categories ) {
	push @category, ($data =~ m#$pat1\b$c\b$pat2#i) ? $1 : '-';
    }
    # correction for NCBI mistake.
    $category[0] =~ s/Actinopteri/Actinopterygii/;
    # add common name in front
    unshift @category, $data =~ m#common name:.*?<strong>(.*?)</strong># ? $1 : '-';
    return @category;
}


#$species = shift;

for my $species (<*>) {
    next unless -d $species;
    next if $species =~ /^\./;
    chomp $species;
    $species =~ s/_/ /g;
    say join "\t", $species, html_essence fetch_ncbi $species;
}
