#!/usr/bin/perl -w

use feature 'say';
use Digest::MD5 qw/md5_hex/;
use Digest::Nilsimsa;
use MIME::Base64;
my $nilsimsa = Digest::Nilsimsa->new();

open my $fh, "zcat */protein/*.gz | sed '1b;/^>/i\n'|" or die;
$/="";
while (<$fh>) {
  my ($h,$seq) = split /\n/, $_, 2;
  $seq =~ s/\s//g;
  $md5 = md5_hex $seq;
  my $digest = $nilsimsa->text2digest($seq);
  print $md5, "\t",encode_base64 pack "H*", $digest;
  #print $md5, "\t", $digest;
  #say $seq;
}

