#!/usr/bin/perl -w

use feature 'say';
use Digest::MD5 qw/md5_hex/;
#use Digest::Nilsimsa;
#use MIME::Base64;
#my $nilsimsa = Digest::Nilsimsa->new();

my %SIGN;

open my $fh, "zcat */protein/*.gz | sed '1b;/^>/i\n'|" or die;
$/="";
while (<$fh>) {
  my ($h,$seq) = split /\n/, $_, 2;
  $seq =~ s/\s//g;
  $md5 = md5_hex $seq;
  defined $SIGN{$md5} or say "$md5\t$seq";
  $SIGN{$md5} = '1';
#  my $digest = $nilsimsa->text2digest($seq);
  #print $md5, "\t",encode_base64 pack "H*", $digest;
}

